﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class NewsViewModel : BaseViewModel
    {
        public List<News> News { get; set; }
        public List<Product> Products { get; set; }
        public List<Special> Specials { get; set; }
        public string NewsScrollUrl { get; set; }
    }
}