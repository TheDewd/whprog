﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;
using WholesaleHunter.Classes;

namespace WholesaleHunter.ViewModels
{
    public class BaseViewModel
    {
        public int CustomerID { get; set; }
        public CartPreview Cart { get; set; }

        public string OG_Image { get; set; }
        public string OG_Title { get; set; }
        public string OG_Url { get; set; }
        public string OG_Description { get; set; }

        public BaseViewModel()
        {
            this.OG_Image = "";
            this.OG_Title = "";
            this.OG_Url = "";
            this.OG_Description = "";
        }
    }
}