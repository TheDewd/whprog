﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class UserProfileViewModel : BaseViewModel
    {
        public string ErrorMessage { get; set; }
        public List<MonthlySalesSummary> MonthlySummary { get; set; }
        public List<MonthlySalesDetail> MonthlyDetails { get; set; }

        public UserProfileViewModel()
        {
            this.MonthlySummary = new List<MonthlySalesSummary>();
            this.MonthlyDetails = new List<MonthlySalesDetail>();
        }
    }
}