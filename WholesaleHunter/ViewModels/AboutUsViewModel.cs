﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class AboutUsViewModel : BaseViewModel
    {
        public List<Special> Specials { get; set; }
    }
}