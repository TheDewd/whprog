﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class CartFFLViewModel : BaseViewModel
    {
        public List<CartFFL> listFFL { get; set; }
        public CartPrimaryFFL PrimaryFFL { get; set; }
        public string JSONFFL { get; set; }
        public string FFLName { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }

        public String Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }

        public CartFFLViewModel()
        {
            listFFL = new List<CartFFL>();
        }
    }
}