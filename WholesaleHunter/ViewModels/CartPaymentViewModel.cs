﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class CartPaymentViewModel : CartViewModel
    {
        public int FFLStatus { get; set; }
        public string Message { get; set; }
        public decimal Tax { get; set; }
        public decimal ShippingInsurancePrice { get; set; }
        public bool IsRegistered { get; set; }
        public string Email { get; set; }
        public int CartInventoryCheck { get; set; }
        public string CartAdjustedMessage { get; set; }
        public int OrderNumber { get; set; }
        public List<CartShippingMethod> ShippingMethods { get; set; }
        public List<CartPaymentMethod> PaymentMethods { get; set; }
        public AlmostThere AlmostThere { get; set; }
        public CartMyInfo CartInfo { get; set; }
        public BillingAddress CreditCardBillingAddress { get; set; }
        public bool ShippingCostIsNull { get; set; }

        public CartPaymentViewModel()
        {
            items = new List<CartListItem>();
            suggested = new List<CartSuggestedProducts>();
            ShippingMethods = new List<CartShippingMethod>();
            PaymentMethods = new List<CartPaymentMethod>();
            ShippingCostIsNull = false;
        }

        public CartPaymentViewModel(CartViewModel cvm)
        {
            this.Cart = cvm.Cart;                           //defined in BaseViewModel
            this.CustomerID = cvm.CustomerID;               //defined in BaseViewModel
            this.items = cvm.items;
            this.shipping = cvm.shipping;
            this.ShippingAmount = cvm.ShippingAmount;
            this.suggested = cvm.suggested;
            this.TaxAmount = cvm.TaxAmount;
            this.TaxDisplay = cvm.TaxDisplay;
            this.HazmatAmount = cvm.HazmatAmount;
            this.HazmatDisplay = cvm.HazmatDisplay;
            this.ShippingMethods = new List<CartShippingMethod>();
            this.PaymentMethods = new List<CartPaymentMethod>();
        }

        public decimal GetCartSubTotalWithDefaultShipping()
        {
            decimal total = this.GetCartSubtotal();
            
            //now add in the default shipping amount
            CartShippingMethod defaultShipping = this.ShippingMethods.First<CartShippingMethod>();
            decimal ShippingCost;
            decimal.TryParse(defaultShipping.ShippingCost, out ShippingCost);
            total += ShippingCost;

            //now add in the hazmat amount
            total += this.HazmatAmount;

            //and add in the shipping insurance
            total += this.ShippingInsurancePrice;

            return total;
        }

        public decimal GetCartTotalWithTax()
        {
            decimal total = this.GetCartSubTotalWithDefaultShipping();
            total += this.Tax;
            return total;
        }
    }
}