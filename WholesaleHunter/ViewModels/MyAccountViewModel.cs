﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class MyAccountViewModel : BaseViewModel
    {
        public List<MyAddress> CustomerAddress { get; set; }

        public MyAddress NewCustomerAddress { get; set; }

        public List<MyPaymentMethod> CustomerPaymentMethod { get; set; }

        public MyPaymentMethod NewCustomerPaymentMethod { get; set; }

        public List<MyOrderHistory> MyOrderHistory { get; set; }

        public bool HasOrderHistory { get; set; }

        public List<RecentlyViewed> RecentlyViewed { get; set; }

        public string Phone { get; set; }

        public string EMail { get; set; }

        public bool GetNewsletter { get; set; }

        public string OrderScrollUrl { get; set; }

        public int SelectedTab { get; set; }

        public MyInfo MyInfo { get; set; }
    }
}