﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class CartCompleteViewModel : BaseViewModel
    {

        public int OrderNumber { get; set; }
        public ChargeStatus Status { get; set; }

        public bool PaidByCheck { get; set; }

        public string ConfirmationEmailHTML { get; set; }

        public int EmailSent { get; set; }
        public string EmailAddress { get; set; }

        public CartCompleteViewModel()
        {
            this.PaidByCheck = false;
        }
    }
}