﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class MyInfoViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public CartMyInfo MyInfo { get; set; }
        public List<CartMyAddress> Addresses { get; set; }
        public string JSONAddresses { get; set; }
        public int FFLRequired { get; set; }
        public string IsRegistered { get; set; }
        public string IsNewUser { get; set; }
        public string IsGuest { get; set; }

        public MyInfoViewModel()
        {
            this.MyInfo = new CartMyInfo();
            this.Addresses = new List<CartMyAddress>();
            this.IsRegistered = "false";
            this.IsNewUser = "false";
            this.IsGuest = "false";
        }
    }
}