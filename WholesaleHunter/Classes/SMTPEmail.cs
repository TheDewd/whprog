﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Text;

namespace WholesaleHunter.Classes
{
    public class SMTPEmail
    {
        private MailMessage mail { get; set; }
        private SmtpClient client { get; set; }

        public void Compose(string subject, MailAddress to, MailAddress from, string body, bool isHTML=false)
        {
            this.mail = new MailMessage();
            this.mail.IsBodyHtml = isHTML;
            this.mail.From = from;
            this.mail.To.Add(to);
            this.mail.Subject = subject;
            this.mail.Body = body;
        }

        public SMTPEmail()
        {
            this.client = new SmtpClient();
            this.client.Port = Int32.Parse(ConfigurationManager.AppSettings["SMTP_Port"]);
            this.client.Host = ConfigurationManager.AppSettings["SMTP_Server"];
            this.client.Timeout = 10000;
            this.client.DeliveryMethod = SmtpDeliveryMethod.Network;
            this.client.UseDefaultCredentials = false;
            this.client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_Username"], ConfigurationManager.AppSettings["SMTP_Password"]);

            bool success;
            bool enableSsl = Boolean.TryParse(ConfigurationManager.AppSettings["SMTP_EnableSSL"], out success);
            if (success)
            {
                //the parse was succesful
                this.client.EnableSsl = enableSsl;
            }
            else
            {
                //the parse was not succesful
                this.client.EnableSsl = false;
            }
        }

        public void Send()
        {
            this.client.Send(this.mail);
        }
    }
}