﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace WholesaleHunter.Classes
{
    public class StatesList
    {
        public List<StateInfo> States { get; set; }

        public StatesList()
        {
            States = new List<StateInfo>();
            States.Add(new StateInfo("Alabama", "AL"));
            States.Add(new StateInfo("Alaska", "AK"));
            States.Add(new StateInfo("Arizona", "AZ"));
            States.Add(new StateInfo("Arkansas", "AR"));
            States.Add(new StateInfo("California", "CA"));
            States.Add(new StateInfo("Colorado", "CO"));
            States.Add(new StateInfo("Connecticut", "CT"));
            States.Add(new StateInfo("Delaware", "DE"));
            States.Add(new StateInfo("District Of Columbia", "DC"));
            States.Add(new StateInfo("Florida", "FL"));
            States.Add(new StateInfo("Georgia", "GA"));
            States.Add(new StateInfo("Hawaii", "HI"));
            States.Add(new StateInfo("Idaho", "ID"));
            States.Add(new StateInfo("Illinois", "IL"));
            States.Add(new StateInfo("Indiana", "IN"));
            States.Add(new StateInfo("Iowa", "IA"));
            States.Add(new StateInfo("Kansas", "KS"));
            States.Add(new StateInfo("Kentucky", "KY"));
            States.Add(new StateInfo("Louisiana", "LA"));
            States.Add(new StateInfo("Maine", "ME"));
            States.Add(new StateInfo("Maryland", "MD"));
            States.Add(new StateInfo("Massachusetts", "MA"));
            States.Add(new StateInfo("Michigan", "MI"));
            States.Add(new StateInfo("Minnesota", "MN"));
            States.Add(new StateInfo("Mississippi", "MS"));
            States.Add(new StateInfo("Missouri", "MO"));
            States.Add(new StateInfo("Montana", "MT"));
            States.Add(new StateInfo("Nebraska", "NE"));
            States.Add(new StateInfo("Nevada", "NV"));
            States.Add(new StateInfo("New Hampshire", "NH"));
            States.Add(new StateInfo("New Jersey", "NJ"));
            States.Add(new StateInfo("New Mexico", "NM"));
            States.Add(new StateInfo("New York", "NY"));
            States.Add(new StateInfo("North Carolina", "NC"));
            States.Add(new StateInfo("North Dakota", "ND"));
            States.Add(new StateInfo("Ohio", "OH"));
            States.Add(new StateInfo("Oklahoma", "OK"));
            States.Add(new StateInfo("Oregon", "OR"));
            States.Add(new StateInfo("Pennsylvania", "PA"));
            States.Add(new StateInfo("Rhode Island", "RI"));
            States.Add(new StateInfo("South Carolina", "SC"));
            States.Add(new StateInfo("South Dakota", "SD"));
            States.Add(new StateInfo("Tennessee", "TN"));
            States.Add(new StateInfo("Texas", "TX"));
            States.Add(new StateInfo("Utah", "UT"));
            States.Add(new StateInfo("Vermont", "VT"));
            States.Add(new StateInfo("Virginia", "VA"));
            States.Add(new StateInfo("Washington", "WA"));
            States.Add(new StateInfo("West Virginia", "WV"));
            States.Add(new StateInfo("Wisconsin", "WI"));
            States.Add(new StateInfo("Wyoming", "WY"));
        }
    }
}