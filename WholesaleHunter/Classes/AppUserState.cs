﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace WholesaleHunter.Classes
{
    public class AppUserState
    {
        public int UserID { get; set; }
        public string Email { get; set; }


        public AppUserState()
        {

        }


        /// <summary>
        /// Exports a short string list of Id, Email, Name separated by |
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.UserID.ToString());
            sb.Append("|");
            sb.Append(this.Email);
            return sb.ToString();
        }

        public bool FromString(string itemString)
        {
            if (string.IsNullOrEmpty(itemString))
                return false;

            string[] strings = itemString.Split('|');
            if (strings.Length < 2)
                return false;

            this.UserID = Int32.Parse(strings[0]);
            this.Email = strings[1];

            return true;
        }
    }
}