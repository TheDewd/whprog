﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WholesaleHunter.Models;
using Newtonsoft.Json;
using WholesaleHunter.Classes;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Text;
using System.Globalization;

namespace WholesaleHunter.Classes
{
    public class Cart
    {
        public int CustomerID { get; set; }
        public List<CartListItem> CartItems;
        public CartShipping Shipping;
        public List<CartSuggestedProducts> SuggestedProducts;
        public bool IsNewCustomer = false;
        public int NewCustomerID = 0;
        public string ErrorMessage = "";

        private string dbname = "WholesaleHunterCom";
        private List<CartListItem> tempItems;

        public Cart(int CustomerID)
        {
            this.CustomerID = CustomerID;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //first, get the current cart contents
                var p = new DynamicParameters();
                p.Add("@CustomerID", this.CustomerID);
                var results = db.QueryMultiple("WholesaleHunterCom.sShoppingCart", p, commandType: CommandType.StoredProcedure);

                CartItems = results.Read<CartListItem>().ToList();
                Shipping = results.Read<CartShipping>().Single();
                SuggestedProducts = results.Read<CartSuggestedProducts>().ToList();
            }
        }

        public int UpdateCart(CartItemUpdate item)
        {
            int qtyInCartAtThisPrice = 0;
            int qtyAvailableAtThisPrice = 0;
            List<PricePoints> listPrices;
            if (item.NewQuantity < 0)
            {
                this.ErrorMessage = "Error: Quantity must be a positive number.";
                return -1;
            }
            /*
            //get the price points for the item to be added
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@ProductID", item.ProductID);
                var data = db.Query<PricePoints>("WholesaleHunterCom.sProductPrices", p, commandType: CommandType.StoredProcedure);
                listPrices = data.ToList<PricePoints>();
            }

            decimal lowestPrice = 0;
            foreach(PricePoints pp in listPrices)
            {
                if(lowestPrice==0)
                {
                    lowestPrice = pp.WHPrice;
                }
                if(pp.WHPrice == item.Price)
                {
                    qtyAvailableAtThisPrice = pp.Qty;
                    break;
                }
            }

            if(item.NewQuantity > qtyAvailableAtThisPrice)
            {
                NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
                nfi = (NumberFormatInfo)nfi.Clone();

                this.ErrorMessage = String.Format(nfi, "Error: There are only {0} units available at the price of {1:C}", qtyAvailableAtThisPrice, item.Price);
                return -1;
            }
            */
            int results;
            int adjustStatus = 0;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@ProductID", item.ProductID);
                p.Add("@CustomerID", item.CustomerID);
                p.Add("@Price", item.Price);
                p.Add("@NewQuantity", item.NewQuantity);
                results = db.Query<int>("WholesaleHunterCom.uCartQuantity", p, commandType: CommandType.StoredProcedure).Single<int>();

                p = new DynamicParameters();
                p.Add("CustomerID", item.CustomerID);
                adjustStatus = db.Query<int>("WholesaleHunterCom.diuVerifyCartInventory", p, commandType: CommandType.StoredProcedure).Single<int>();
                // adjustStatus results: 1: no problems, 0: general failure, -1: qty was reduced, -2: some qty shifted to a higher price
            }

            /*
            int resultsRefresh = -1;
            if(results==1)
            {
                resultsRefresh = RefreshCartItem(item.ProductID, item.CustomerID, lowestPrice);
            }
            */

            return adjustStatus;
        }

        public int RefreshCartItem(int ProductID, int CustomerID, decimal Price)
        {
            int results = -1;
            int resultsAddItem = -1;
            int cntItems = 0;
            try
            {
                //get the count of items currently in the cart
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", CustomerID);
                    p.Add("@ProductID", ProductID);
                    cntItems = db.Query<int>("WholesaleHunterCom.dItemFromCart", p, commandType: CommandType.StoredProcedure).Single<int>();
                }

                if (cntItems >= 0)
                {
                    //get rid of items stored in this object
                    this.CartItems = new List<CartListItem>();

                    CartItem ci = new CartItem();
                    ci.CustomerID = CustomerID;
                    ci.ProductID = ProductID;
                    ci.Price = Price;
                    ci.Quantity = cntItems;
                    results = AddItemToCart(ci);
                }
                else
                {
                    results = -1;
                }
            }
            catch
            {
                results = -1;
            }
            return results;
        }




        public int AddItemToCart(CartItem item)
        {
            int qtyTotalAvailable = 0;
            int qtyInCart = 0;
            List<PricePoints> listPrices;

            if (item.CustomerID == 0)
            {
                this.IsNewCustomer = true;
            }

            //get the price points for the item to be added
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@ProductID", item.ProductID);
                var data = db.Query<PricePoints>("WholesaleHunterCom.sProductPrices", p, commandType: CommandType.StoredProcedure);
                listPrices = data.ToList<PricePoints>();
            }

            //now determine the total quantity available
            foreach (PricePoints pp in listPrices)
            {
                qtyTotalAvailable += pp.Qty;
            }

            //now determine how many items are already in cart
            List<CartListItem> tempItems = new List<CartListItem>();
            foreach (CartListItem itm in CartItems)
            {
                if (itm.ProductID == item.ProductID)
                {
                    tempItems.Add(itm);
                    qtyInCart += itm.Quantity;
                }
            }

            //check to make sure the inventory count isn't exceeded
            if ((qtyInCart + item.Quantity) > qtyTotalAvailable)
            {
                this.ErrorMessage = "Error: There is not enough inventory remaining to fulfill your request.";
                return -1;
            }

            //now step through the items in the cart to see how much of each item is available at each price point
            foreach (CartListItem cartitem in tempItems)
            {
                //find the price point for this line item
                foreach (PricePoints pp in listPrices)
                {
                    if (cartitem.Price == pp.WHPrice)
                    {
                        pp.Check = pp.Check + cartitem.Quantity;
                    }
                }
            }

            //now we want to check the quantity passed in to see which pricepoints are available
            int qtyRequested = item.Quantity;
            int qtyRemaining = item.Quantity;
            int qtyAvailableAtPrice = 0;
            while (qtyRemaining > 0)
            {
                foreach (PricePoints pp in listPrices)
                {
                    qtyAvailableAtPrice = pp.Qty - pp.Check;
                    if (qtyAvailableAtPrice > 0)
                    {
                        //this price point has availability
                        if (qtyRemaining <= qtyAvailableAtPrice)
                        {
                            //this price point can satisfy the entire request.
                            int insertresults = InsertIntoCart(item.CustomerID, item.ProductID, pp.WHPrice, qtyRemaining, item.SourceID);
                            if (insertresults < 0)
                            {
                                //an error was encountered
                                ErrorMessage = "ERROR A";
                                return -1;
                            }
                            else
                            {
                                if (this.IsNewCustomer && this.NewCustomerID == 0)
                                {
                                    this.NewCustomerID = insertresults;
                                    item.CustomerID = insertresults;
                                }
                            }
                            qtyRemaining = 0;
                            break;
                        }
                        else
                        {
                            //this price point can partially satisfy the request
                            int insertresults = InsertIntoCart(item.CustomerID, item.ProductID, pp.WHPrice, qtyAvailableAtPrice, item.SourceID);
                            qtyRemaining = qtyRemaining - qtyAvailableAtPrice;
                            if (insertresults < 0)
                            {
                                //an error was encountered
                                ErrorMessage = "ERROR A";
                                return -1;
                            }
                            else
                            {
                                if (this.IsNewCustomer && this.NewCustomerID == 0)
                                {
                                    this.NewCustomerID = insertresults;
                                    item.CustomerID = insertresults;
                                }
                            }
                        }
                    }
                }
            }

            return 0;
        }


        private int InsertIntoCart(int customerid, int productid, decimal price, int quantity, string SourceID=null)
        {
            int result;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", customerid);
                p.Add("@ProductID", productid);
                p.Add("@Price", price);
                p.Add("@Quantity", quantity);
                if(SourceID!=null)
                {
                    p.Add("@Source", SourceID);
                }
                result = db.Query<int>("WholesaleHunterCom.iCart", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
            }
            return result;
        }
    }
}