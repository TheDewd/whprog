﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace WholesaleHunter.Classes
{
    public class StateInfo
    {
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }


        public StateInfo(string name, string abbv)
        {
            this.StateName = name;
            this.StateAbbreviation = abbv;
        }
    }
}