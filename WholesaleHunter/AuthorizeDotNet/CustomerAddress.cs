﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class CustomerAddress
    {
        public int CustomerID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string FaxNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public CustomerAddress()
        {            //put string.Empty in all the CustomerAddressType fields
            this.Address = string.Empty;
            this.City = string.Empty;
            this.Company = string.Empty;
            this.Country = string.Empty;
            this.Email = string.Empty;
            this.FaxNumber = string.Empty;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.PhoneNumber = string.Empty;
            this.State = string.Empty;
            this.Zip = string.Empty;
        }
    }
}
