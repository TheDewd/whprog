﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class TransactionResult
    {
        public string TransactionID { get; set; }
        public string ResponseCode { get; set; }
        public string AuthCode { get; set; }
        public string AVSResultCode { get; set; }
        public string CAVVResultCode { get; set; }
        public string CVVResultCode { get; set; }
        public int TransactionResultCode { get; set; }
        public string ErrorCode { get; set; }
        public List<TransactionMessage> Messages { get; set; }
        public List<TransactionError> Errors { get; set; }

        public TransactionResult()
        {
            this.Messages = new List<TransactionMessage>();
            this.Errors = new List<TransactionError>();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("TransactionID: ");
            sb.Append(TransactionID);
            sb.Append("\n");

            sb.Append("ResponseCode: ");
            sb.Append(ResponseCode);
            sb.Append("\n");

            sb.Append("AuthCode: ");
            sb.Append(AuthCode);
            sb.Append("\n");

            sb.Append("AVSResultCode: ");
            sb.Append(AVSResultCode);
            sb.Append("\n");

            sb.Append("CAVVResultCode: ");
            sb.Append(CAVVResultCode);
            sb.Append("\n");

            sb.Append("CVVResultCode: ");
            sb.Append(CAVVResultCode);
            sb.Append("\n");

            sb.Append("TransactionResultCode: ");
            sb.Append(TransactionResultCode);
            sb.Append("\n");

            sb.Append(GetMessages());
            return sb.ToString();
        }

        public string GetMessages()
        {
            StringBuilder messages = new StringBuilder();
            foreach(TransactionMessage msg in Messages)
            {
                messages.Append(msg.code);
                messages.Append(" ");
                messages.Append(msg.description);
                messages.Append("; ");
            }

            foreach(TransactionError err in Errors)
            {
                messages.Append("Error ");
                messages.Append(err.errorCode);
                messages.Append(" ");
                messages.Append(err.errorText);
                messages.Append("; ");
            }
            return messages.ToString();
        }
    }
}
