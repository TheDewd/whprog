﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class CustomerProfileResult {
        public int ResultCode { get; set; }
        public string CustomerProfileId { get; set; }
        public List<string> CustomerPaymentProfileList { get; set; }
        public List<Message> Messages { get; set; }

        public CustomerProfileResult()
        {
            this.CustomerPaymentProfileList = new List<string>();
            this.Messages = new List<Message>();
        }
    }
}
