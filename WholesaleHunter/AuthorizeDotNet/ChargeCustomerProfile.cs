﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;

namespace WholesaleHunter.AuthorizeDotNet
{
    class ChargeCustomerProfile
    {
        public ChargeCustomerProfile()
        {
        }

        public TransactionResult ChargeProfile(string customerProfileId, string customerPaymentProfileId, ChargeInformation chargeInfo)
        {
            TransactionResult transactionResult = new TransactionResult();

            if (ConfigurationManager.AppSettings["AuthorizeNetUseSandbox"].ToLower() == "true")
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetAPILogin"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"],
            };

            //create a customer payment profile
            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = customerProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = customerPaymentProfileId };

            /*
            var billingAddress = new customerAddressType
            {
                firstName = chargeInfo.BillTo.FirstName,
                lastName = chargeInfo.BillTo.LastName,
                address = chargeInfo.BillTo.Address,
                city = chargeInfo.BillTo.City,
                state = chargeInfo.BillTo.State,
                zip = chargeInfo.BillTo.Zip,
                country = chargeInfo.BillTo.Country,
                email = chargeInfo.BillTo.Email,
                phoneNumber = chargeInfo.BillTo.PhoneNumber,
                faxNumber = chargeInfo.BillTo.FaxNumber,
                company = chargeInfo.BillTo.Company
            };
            */

            var shippingAddress = new customerAddressType
            {
                firstName = chargeInfo.ShipTo.FirstName,
                lastName = chargeInfo.ShipTo.LastName,
                address = chargeInfo.ShipTo.Address,
                city = chargeInfo.ShipTo.City,
                state = chargeInfo.ShipTo.State,
                zip = chargeInfo.ShipTo.Zip,
                country = chargeInfo.ShipTo.Country,
                email = chargeInfo.ShipTo.Email,
                phoneNumber = chargeInfo.ShipTo.PhoneNumber,
                faxNumber = chargeInfo.ShipTo.FaxNumber,
                company = chargeInfo.ShipTo.Company
            };

            var orderInfo = new orderType
            {
                invoiceNumber = chargeInfo.OrderID.ToString(),
                description = chargeInfo.OrderDescription
            };

            /*
            var customerInfo = new customerDataType
            {
                type = AuthorizeNet.Api.Contracts.V1.customerTypeEnum.individual,
                id = chargeInfo.CustomerID.ToString(),
                email = chargeInfo.BillTo.Email,
            };
            */

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(), 
                amount = chargeInfo.TransactionAmount,
                profile = profileToCharge,
                shipTo = shippingAddress,
                order = orderInfo
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the collector that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response != null)
            {
                if (response.messages != null)
                {
                    transactionResult.TransactionID = response.transactionResponse.transId;
                    transactionResult.ResponseCode = response.transactionResponse.responseCode;
                    transactionResult.AuthCode = response.transactionResponse.authCode;
                    transactionResult.TransactionResultCode = (int)response.messages.resultCode;
                    transactionResult.AVSResultCode = response.transactionResponse.avsResultCode;
                    transactionResult.CAVVResultCode = response.transactionResponse.cavvResultCode;
                    transactionResult.CVVResultCode = response.transactionResponse.cvvResultCode;

                    if (response.transactionResponse.errors == null)
                    {
                        transactionResult.ErrorCode = "1";
                    }
                    else
                    {
                        foreach (transactionResponseError err in response.transactionResponse.errors)
                        {
                            transactionResult.ErrorCode = err.errorCode;
                            break;
                        }
                    }

                    if (response.transactionResponse.messages != null)
                    {
                        for (int cnt = 0; cnt < response.transactionResponse.messages.Length; cnt++)
                        {
                            TransactionMessage msg = new TransactionMessage();
                            msg.code = response.transactionResponse.messages[cnt].code;
                            msg.description = response.transactionResponse.messages[cnt].description;
                            transactionResult.Messages.Add(msg);
                        }
                    }
                }

                if (response.transactionResponse.errors != null)
                {
                    for (int cnt = 0; cnt < response.transactionResponse.errors.Length; cnt++)
                    {
                        TransactionError err = new TransactionError();
                        err.errorCode = response.transactionResponse.errors[cnt].errorCode;
                        err.errorText = response.transactionResponse.errors[cnt].errorText;
                        transactionResult.Errors.Add(err);
                    }

                }
            }
            return transactionResult;
        }
    }
}
