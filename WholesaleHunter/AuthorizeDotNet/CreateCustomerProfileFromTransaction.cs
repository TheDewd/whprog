﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;

namespace WholesaleHunter.AuthorizeDotNet
{
    class CreateCustomerProfileFromTransaction
    {

        public CreateCustomerProfileFromTransaction()
        {

        }

        public CustomerProfileResult createProfile(CustomerProfileBase profileBase)
        {
            if (ConfigurationManager.AppSettings["AuthorizeNetUseSandbox"].ToLower() == "true")
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetAPILogin"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"],
            };

            var customerProfile = new customerProfileBaseType
            {
                merchantCustomerId = profileBase.CustomerID.ToString(),
                email = profileBase.Email,
                description = profileBase.Description
            };

            var request = new createCustomerProfileFromTransactionRequest
            {
                transId = profileBase.TransactionID,
                // You can either specify the customer information in form of customerProfileBaseType object
                customer = customerProfile
                //  OR   
                // You can just provide the customer Profile ID
                // customerProfileId = "123343"                
            };

            var controller = new createCustomerProfileFromTransactionController(request);
            controller.Execute();

            createCustomerProfileResponse response = controller.GetApiResponse();

            CustomerProfileResult result = new CustomerProfileResult();
            //validate
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                result.ResultCode = (int)response.messages.resultCode;
                result.CustomerProfileId = response.customerProfileId;
                for (int cnt = 0; cnt < response.customerPaymentProfileIdList.Length; cnt++)
                {
                    result.CustomerPaymentProfileList.Add(response.customerPaymentProfileIdList[cnt]);
                }
            }
            else if (response != null)
            {
                result.ResultCode = (int)response.messages.resultCode;
                for (int cnt = 0; cnt < response.messages.message.Length; cnt++)
                {
                    Message msg = new Message();
                    msg.code = response.messages.message[cnt].code;
                    msg.text = "Error: " + response.messages.message[cnt].text;
                }
            }

            return result;
        }
    }
}
