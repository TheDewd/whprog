﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class TransactionError
    {
        public string errorCode { get; set; }
        public string errorText { get; set; }
    }
}
