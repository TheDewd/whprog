﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class TransactionMessage
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}
