﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net.Mail;
using System.Text;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using FormCollection = System.Web.Mvc.FormCollection;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using WholesaleHunter.Classes;
using WholesaleHunter.Models;

namespace WholesaleHunter.Controllers
{

    [HandleError]
    [OutputCache(Duration = 1)]
    public class AccountController : Controller
    {

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        [AllowAnonymous]
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(NewAccount data)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["WholesaleHunterCom"].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@Username", data.Email);
                p.Add("@Password", data.Password);
                p.Add("@FirstName", data.FirstName);
                p.Add("@LastName", data.LastName);
                p.Add("@Address1", data.Address1);
                p.Add("@Address2", data.Address2);
                p.Add("@City", data.City);
                p.Add("@State", data.State);
                p.Add("@ZIPCode", data.Zip);
                p.Add("@Phone", data.Phone);
                int userid = db.Query<int>("WholesaleHunterCom.iLoginCreate", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if(userid==-1)
                {
                    //the email address is already in use
                }
                //create a Logon object and send to Action/LogOn
            }
            return Redirect("/Account/PasswordSent");
        }


        #region Indivdual User Cookie Logins
        [AllowAnonymous]
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogOn(Logon data)
        {
            string returnUrl = "";
            Login user;

            if (data.forgotPW == "1")
            {
                int err = 0;
                string forgottenpw = "";
                try
                {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["WholesaleHunterCom"].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@Username", data.username);
                    PasswordEmail pword = db.Query<PasswordEmail>("WholesaleHunterCom.sPasswordEmail", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    forgottenpw = pword.Password;
                }

                MailAddress To = new MailAddress(data.username);
                MailAddress From = new MailAddress(ConfigurationManager.AppSettings["LostPasswordMailFrom"]);
                string Subject = ConfigurationManager.AppSettings["LostPasswordMailSubject"];

                StringBuilder body = new StringBuilder();
                body.Append(ConfigurationManager.AppSettings["LostPasswordMessageText"]);
                body.Append(forgottenpw);

                SMTPEmail mailer = new SMTPEmail();
                mailer.Compose(Subject, To, From, body.ToString());
                mailer.Send();
                }
                 catch
                 {
                     err = 1;
                 }

                if (err == 1)
                {
                    return Redirect("/Account/ErrorSendingPassword");
                }
                else
                {
                    return Redirect("/Account/PasswordSent");
                }
            }

            //At this point, check the db and see if the user is present
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["WholesaleHunterCom"].ConnectionString))
            {

                var p = new DynamicParameters();
                p.Add("@Username", data.username);
                p.Add("@Password", data.password);

                user = db.Query<Login>("[WholesaleHunterCom].[sLoginVerify]", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (user == null)
                {
                    return Redirect("/Account/Failed");
                }

                if (user.CustomerID == 0)
                {
                    return Redirect("/Account/Failed");
                }
            }

            AppUserState appUserState = new AppUserState();
            appUserState.Email = data.username;
            appUserState.UserID = user.CustomerID;


            //IssueAuthTicket(appUserState, rememberMe);

            IdentitySignin(appUserState, "providerKey");

            //if (!string.IsNullOrEmpty(returnUrl))
            //    return Redirect(returnUrl);

            return Redirect("/Home/Index");

        }

        public ActionResult LogOff()
        {
            IdentitySignout();
            return RedirectToAction("", "Login");
        }

        public ActionResult Register(string id)
        {
            return Redirect("url to go to");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateAntiForgeryToken]
        public ActionResult Register(FormCollection formVars)
        {
            return Redirect("url to go to");
        }

        #endregion

        // **** Helpers 

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "CodePaste_$31!.2*#";

        public class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                    properties.Dictionary[XsrfKey] = UserId;

                var owin = context.HttpContext.GetOwinContext();
                owin.Authentication.Challenge(properties, LoginProvider);
            }
        }


        #region SignIn and Signout

        /// <summary>
        /// Helper method that adds the Identity cookie to the request output
        /// headers. Assigns the userState to the claims for holding user
        /// data without having to reload the data from disk on each request.
        /// 
        /// AppUserState is read in as part of the baseController class.
        /// </summary>
        /// <param name="appUserState"></param>
        /// <param name="providerKey"></param>
        /// <param name="isPersistent"></param>
        public void IdentitySignin(AppUserState appUserState, string providerKey = null)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.Email, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.Name, appUserState.Email));
            // serialized AppUserState object
            claims.Add(new Claim("userState", appUserState.ToString()));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;

            // add to user here!
            int ExpireDays = Int32.Parse(ConfigurationManager.AppSettings["ExpireAuthenticationCookieInDays"]);
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            };

            AuthenticationManager.SignIn(authProperties, identity);
        }

        public void IdentitySignout()
        {
            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalCookie);
        }

        /*private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }
        */
        #endregion

        /*
        public ActionResult EmailPassword(string email)
        {
            User user = busUser.LoadUserByEmail(email);
            if (user == null)
                ErrorDisplay.ShowError(
                    "Email address doesn't exist. Please make sure you have typed the address correctly");
            else
            {
                // Always create a new random password
                string password = StringUtils.NewStringId();
                user.Password = App.EncodePassword(password, user.Id);
                busUser.Save();

                if (AppWebUtils.SendEmail(App.Configuration.ApplicationTitle + " Email Information",
                    "Your CodePaste account password is: " + password + "\r\n\r\n" +
                    "You can log on at: " + WebUtils.GetFullApplicationPath() + "\r\n\r\n" +
                    "Please log in, view your profile and then change your password to something you can more easily remember.",
                    busUser.Entity.Email,
                    true))
                    ErrorDisplay.ShowMessage("Password information has been emailed to: " + busUser.Entity.Email);
                else
                    ErrorDisplay.ShowError("Emailing of password failed.");
            }

            return View(ViewModel);
        }


        //[AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ValidateEmail(string id = null)
        {
            var user = busUser.ValidateEmailAddress(id);
            if (user == null)
                throw new ApplicationException("Invalid email validator id.");

            return RedirectToAction("New", "Snippet",
                new { message = "You're ready to post: Your email address is validated." });
        }

        public ActionResult ResetEmailValidation()
        {
            if (string.IsNullOrEmpty(AppUserState.UserId))
                return new HttpUnauthorizedResult();

            var user = busUser.Load(AppUserState.UserId);

            SetAccountForEmailValidation();

            ErrorDisplay.ShowMessage(
                @"An email has been sent to validate your email address.
Please follow the instructions in the email to validate
your email address.");

            return Register(user.Id);
        }


        /// <summary>
        /// Makes user account inactive, sets a new validator
        /// and then emails the user.
        /// Assume busUser.Entity is set.
        /// </summary>        
        private void SetAccountForEmailValidation()
        {
            busUser.SetUserForEmailValidation();
            busUser.Save();

            var msg = string.Format(
                @"In order to validate your email address for CodePaste.net, please click or paste the
following link into your browser's address bar: 
{0}
Once you click the link you're ready to create new
code pastes with your account on the CodePaste.net site.
Sincerely,
The CodePaste.net Team
", WebUtils.ResolveServerUrl("~/Account/ValidateEmail/" + busUser.Entity.Validator));

            AppWebUtils.SendEmail("Codepaste.net Email Validation",
                msg, busUser.Entity.Email, true);
        }

    
        */
    }

}