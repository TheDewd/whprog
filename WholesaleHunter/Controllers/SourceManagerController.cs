﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Dapper;
using WholesaleHunter.Classes;
using WholesaleHunter.Models;
using WholesaleHunter.ViewModels;

namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    public class SourceManagerController : Controller
    {
        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";

        // GET: SourceManager
        [Authorize]
        public ActionResult UserProfile()
        {
            int CustomerID;
            string Email;
            UserProfileViewModel vm = new UserProfileViewModel();
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                Email = this.appuser.Email;
            }
            string ip = Request.UserHostAddress;
            if (ip == "58.187.179.76")
            {
                vm.ErrorMessage = "Your IP address is banned.";
                return View(vm);
            }

            //get the monthly sales summary
            string sql;

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                vm.MonthlySummary = db.Query<MonthlySalesSummary>("WholesaleHunterCom.sSourceManagerSummary", null, commandType: CommandType.StoredProcedure).ToList<MonthlySalesSummary>();

                //get the monthly sales details
                vm.MonthlyDetails = db.Query<MonthlySalesDetail>("WholesaleHunterCom.sSourceManagerDetail", null, commandType: CommandType.StoredProcedure).ToList<MonthlySalesDetail>();

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                /*
                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", catid.ToString());
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();
                */
            }

            return View(vm);
        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }
    }
}