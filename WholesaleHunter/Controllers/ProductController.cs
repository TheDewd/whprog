﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using WholesaleHunter.ViewModels;
using WholesaleHunter.Models;
using WholesaleHunter.Classes;
using System.Security.Claims;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json.Linq;

namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    public class ProductController : Controller
    {
        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";

        [HttpGet]
        public ActionResult Details(int ID, string fs, string source)
        {
            int IsNewCustomer;
            int CustomerID;
            string CustomerEmail = "";
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
                CustomerEmail = this.appuser.Email;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
                CustomerEmail = "";
            }

            ProductDetailsViewModel vm = getViewModel(ID, CustomerID);
            vm.Email = CustomerEmail;
            if(fs=="1")
            {
                vm.IsFromSearch = true;
            }
            else
            {
                vm.IsFromSearch = false;
            }
            vm.SourceID = source;
            return View(vm);
        }

        public ActionResult DetailsBySKU(int ID, string fs, string source)
        {
            int IsNewCustomer;
            int CustomerID;
            string CustomerEmail = "";
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
                CustomerEmail = this.appuser.Email;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
                CustomerEmail = "";
            }

            ProductDetailsViewModel vm = getViewModel(ID, CustomerID, true);
            vm.Email = CustomerEmail;
            if (fs == "1")
            {
                vm.IsFromSearch = true;
            }
            else
            {
                vm.IsFromSearch = false;
            }
            vm.SourceID = source;
            return View("Details", vm);
        }
        private ProductDetailsViewModel getViewModel(int ProductID, int CustomerID, bool UseSKU=false)
        {
            ProductDetailsViewModel vm = new ProductDetailsViewModel();
            vm.CustomerID = CustomerID;
            string procedure;
            string parameter;
            string folders;

            if(UseSKU)
            {
                procedure = "WholesaleHunterCom.sProductDetailBySKU";
                parameter = "@SKURcrd";
                folders = "Product/DetailsBySKU/";
            }
            else
            {
                procedure = "WholesaleHunterCom.sProductDetail";
                parameter = "@ProductID";
                folders = "Product/Details/";
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                p.Add(parameter, ProductID);
                p.Add("@CustomerID", CustomerID);
                var results = db.QueryMultiple(procedure, p, commandType: CommandType.StoredProcedure);
                vm.Details = results.Read<ProductDetails>().ToList().FirstOrDefault();
                vm.Images = results.Read<ProductDetailsImages>().ToList();
                vm.Specs = results.Read<ProductDetailsSpecs>().ToList();
                vm.SuggestedProducts = results.Read<Product>().ToList();

                if(UseSKU)
                {
                    vm.ProductID = vm.Details.ProductID;
                }
                else
                {
                    vm.ProductID = ProductID;
                }

                if(vm.Details.mfgWebsite!=null) { 
                    if (!vm.Details.mfgWebsite.Contains("http"))
                    {
                        vm.Details.mfgWebsite = "http://" + vm.Details.mfgWebsite;
                    }
                }

                //check each image to see if it is a complete path
                bool flagMainImage = false;
                string imgbase = ConfigurationManager.AppSettings["imgbase"].ToString();
                if (!imgbase.EndsWith("/"))
                {
                    imgbase += "/";
                }
                foreach (ProductDetailsImages img in vm.Images)
                {
                    if (!flagMainImage)
                    {
                        vm.MainImage = img.Picture;
                        flagMainImage = true;
                    }
                    if (!img.Picture.Contains("http:"))
                    {
                        img.Picture = imgbase + img.Picture;
                    }
                }

                //add the og: meta info
                string host = ConfigurationManager.AppSettings["Host"].ToString();
                vm.OG_Image = host.Substring(0, host.Length-1) + vm.MainImage;
                vm.OG_Title = vm.Details.Title;
                vm.OG_Description = vm.Details.NoHTMLDescription;
                StringBuilder sbUrl = new StringBuilder();
                sbUrl.Append(host);
                sbUrl.Append(folders);
                sbUrl.Append(ProductID);
                vm.OG_Url = sbUrl.ToString();

                //now we need to get the CartPreview
                p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                //now get the Specials
                p = new DynamicParameters();
                p.Add("@IsNewCustomer", vm.CustomerID);
                p.Add("@Category", vm.Details.CategoryID.ToString());
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", p, commandType: CommandType.StoredProcedure).ToList<Special>();

                //now get the price points
                p = new DynamicParameters();
                p.Add("@ProductID", ProductID);
                vm.Prices = db.Query<PricePoints>("WholesaleHunterCom.sProductPrices", p, commandType: CommandType.StoredProcedure).ToList<PricePoints>();

            }
            return vm;
        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }
    }
}