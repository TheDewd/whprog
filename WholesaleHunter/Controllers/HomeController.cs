﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WholesaleHunter.Models;
using WholesaleHunter.ViewModels;
using WholesaleHunter.Classes;
using System.Security.Claims;
using System.Text;

namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    public class HomeController : Controller
    {
        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";

        public ActionResult Index(string id)
        {
            IndexViewModel vm = new IndexViewModel();

            vm.ShowCartOnLoad = false;
            if (id != null) { 
                if(id.ToLower()=="showcart")
                {
                    vm.ShowCartOnLoad = true;
                }
            }

            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
            }
            else
            {
                vm.CustomerID = 0;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                var results = db.QueryMultiple("WholesaleHunterCom.sLoadHome", p, commandType: CommandType.StoredProcedure);

                vm.Specials = results.Read<Special>().ToList();
                vm.News = results.Read<News>().ToList();
                vm.Cart = results.Read<CartPreview>().ToList().First();
                vm.TopSellingInitial = results.Read<Product>().ToList();
                vm.NewProducts = results.Read<Product>().ToList();
                vm.Clearance = results.Read<Product>().ToList();
                vm.RecentlyViewed = results.Read<Product>().ToList();
                vm.Guns = results.Read<Product>().ToList();
                vm.Ammunition = results.Read<Product>().ToList();
                vm.Reloading = results.Read<Product>().ToList();
            }
            ViewBag.Title = "Home Page for Wholesale Hunter";
            ViewBag.Description = "We are Wholesale Hunter. We have been scouring the world for the best deals on guns, ammo and your other hunting and fishing needs since 1998.";

            string hostname = ConfigurationManager.AppSettings["Host"].ToString();
            vm.OG_Title = "Home Page for Wholesale Hunter";
            vm.OG_Description = "We are Wholesale Hunter. We have been scouring the world for the best deals on guns, ammo and your other hunting and fishing needs since 1998.";
            vm.OG_Image = hostname.Substring(0, hostname.Length - 1);
            vm.OG_Url = hostname;
            return View(vm);
        }

        [AllowAnonymous]
        public ActionResult Unsubscribe(string id)
        {
            UnsubscribeViewModel vm = new UnsubscribeViewModel();
            vm.Email = id;

            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
            }
            else
            {
                vm.CustomerID = 0;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //remove the email address from the new letter
                var p = new DynamicParameters();
                p.Add("@Email", id);
                p.Add("@GetNewsletter", 0);
                vm.Status = db.Query<int>("WholesaleHunterCom.uEmailList", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();

                //get the userid
                p = new DynamicParameters();
                p.Add("@CustomerID", 0);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return View(vm);
        }

        public ActionResult GB_Login()
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                AboutUsViewModel vm = new AboutUsViewModel();

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", 0);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", 1);
                pS.Add("@Category", 0);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                return View(vm);
            }
        }

        public ActionResult Terms(string id)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                AboutUsViewModel vm = new AboutUsViewModel();
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();

                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", (int?)null);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                return View(vm);
            }
        }

        public ActionResult GUID(string id)
        {
            System.Guid uniqueid;
            CustomerGUID custinfo;

            if (System.Guid.TryParse(id, out uniqueid))
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    //get the userid
                    var p = new DynamicParameters();
                    p.Add("@GUID", uniqueid);

                    custinfo = db.Query<CustomerGUID>("WholesaleHunterCom.sCustomerIDFromGUID", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }

                AppUserState appUserState = new AppUserState();
                appUserState.Email = custinfo.Email;
                appUserState.UserID = custinfo.CustomerID;

                IdentitySignin(appUserState, "providerKey");
            }
            return RedirectToAction("Index", "Home", new { id = "ShowCart" });
        }

        public ActionResult Empty(string id, int index = 0)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                BaseViewModel vm = new BaseViewModel();

                SearchCriteria criteria = new SearchCriteria();

                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                }
                else
                {
                    vm.CustomerID = 0;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                return View(vm);
            }
        }













        public ActionResult About(string id, int index = 0)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                AboutUsViewModel vm = new AboutUsViewModel();
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();

                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", (int?)null);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                return View(vm);
            }
        }

        public ActionResult Contact(string id, int index = 0)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();

                ContactUsViewModel vm = new ContactUsViewModel();
                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", (int?)null);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                var helpFile = db.QueryMultiple("WholesaleHunterCom.sHelpMain", null, commandType: CommandType.StoredProcedure);
                vm.HelpFile = helpFile.Read<HelpFile>().ToList();
                vm.HelpCategories = helpFile.Read<HelpFile>().ToList();
                vm.FAQ = helpFile.Read<HelpFile>().ToList();
                vm.HelpTopics = helpFile.Read<HelpFile>().ToList();

                return View(vm);
            }
        }

        public ActionResult Error(string id, int index = 0)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();

                ContactUsViewModel vm = new ContactUsViewModel();
                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", catid.ToString());
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                return View(vm);
            }
        }

        public ActionResult SiteMap(string id, int index = 0)
        {

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                int IsNewCustomer;
                SearchCriteria criteria = new SearchCriteria();

                SiteMapViewModel vm = new SiteMapViewModel();
                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //Get the Cart Information
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", catid);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                //Get the Sitemap information
                var sitemapResults = db.QueryMultiple("WholesaleHunterCom.sSiteMapCategories", null, commandType: CommandType.StoredProcedure);
                vm.SiteCategories = sitemapResults.Read<SiteCategory>().ToList();
                vm.DistinctSiteCategories = sitemapResults.Read<SiteCategory>().ToList();
                vm.Products = sitemapResults.Read<SitemapProduct>().ToList();
                return View(vm);
            }
        }

        // GET: Manufacturers
        public ActionResult ShowManufacturers()
        {

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {

                var manufacturers = db.Query<Manufacturer>("WholesaleHunterCom.sManufacturers", commandType: CommandType.StoredProcedure);

                return View(manufacturers);
            }

        }


        public ActionResult Help(string id, int index = 0)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                HelpViewModel vm = new HelpViewModel();
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();


                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).First();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", (int?)null);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                //Get Help Topic, Categories and FAQ's
                var helpFile = db.QueryMultiple("WholesaleHunterCom.sHelpMain", null, commandType: CommandType.StoredProcedure);
                vm.HelpFile = helpFile.Read<HelpFile>().ToList();
                vm.HelpCategories = helpFile.Read<HelpFile>().ToList();
                vm.FAQ = helpFile.Read<HelpFile>().ToList();
                vm.HelpTopics = helpFile.Read<HelpFile>().ToList();


                return View(vm);
            }
        }

        public ActionResult HelpSearchResults(string searchText)
        {

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                HelpViewModel vm = new HelpViewModel();
                var p = new DynamicParameters();
                p.Add("@SearchText", searchText);

                vm.HelpFile = db.Query<HelpFile>("WholesaleHunterCom.sHelpSearch", p, commandType: CommandType.StoredProcedure).ToList<HelpFile>();

                if (vm.HelpFile.Count == 0)
                {
                    var noResults = "Your search for '" + searchText + "' did not yield any results.";
                    HelpFile noHelpResult = new HelpFile();
                    noHelpResult.HelpText = noResults;
                    vm.HelpFile.Add(noHelpResult);
                }
                return View(vm);
            }

        }

        public ActionResult News(string id, int index = 0, int productStartRow = 1, int productEndRow = 5)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                NewsViewModel vm = new NewsViewModel();
                int IsNewCustomer;

                SearchCriteria criteria = new SearchCriteria();

                if (getClaims())
                {
                    vm.CustomerID = this.appuser.UserID;
                    IsNewCustomer = 0;
                }
                else
                {
                    vm.CustomerID = 0;
                    IsNewCustomer = 1;
                }

                int catid;
                if (Int32.TryParse(id, out catid))
                {
                    criteria.CategoryID = catid;
                }

                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);

                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();

                //Get Specials
                var pS = new DynamicParameters();
                pS.Add("@IsNewCustomer", IsNewCustomer);
                pS.Add("@Category", (int?)null);
                vm.Specials = db.Query<Special>("WholesaleHunterCom.sSpecials", pS, commandType: CommandType.StoredProcedure).ToList();

                //Get News Data
                var pN = new DynamicParameters();
                //pN.Add("@StoryStartRow", storyStartRow);
                //pN.Add("@StoryEndRow", storyEndRow);
                pN.Add("@StartRow", productStartRow);
                pN.Add("@EndRow", productEndRow);

                vm.Products = db.Query<Product>("WholesaleHunterCom.sNewProducts", pN, commandType: CommandType.StoredProcedure).ToList<Product>();

                //Build the scrolling URL for the next group of news stories
                NewsScroll newsScroll = new NewsScroll();
                //newsScroll.StartNum = storyStartRow;
                //newsScroll.EndNum = storyEndRow;
                //vm.NewsScrollUrl = buildNextNewsScrollingURL(newsScroll);               

                return View(vm);
            }
        }


        public ActionResult NewsStories(int storyStartRow = 1, int storyEndRow = 3)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                NewsViewModel vm = new NewsViewModel();

                //Get News Data
                var pN = new DynamicParameters();
                pN.Add("@StartRow", storyStartRow);
                pN.Add("@EndRow", storyEndRow);

                vm.News = db.Query<News>("WholesaleHunterCom.sNewsPreview", pN, commandType: CommandType.StoredProcedure).ToList<News>();

                if (vm.News.Count > 0)
                {
                    //Build the scrolling URL for the next group of news stories
                    NewsScroll newsScroll = new NewsScroll();
                    newsScroll.StartNum = storyStartRow;
                    newsScroll.EndNum = storyEndRow;
                    vm.NewsScrollUrl = buildNextNewsScrollingURL(newsScroll);
                }
                else
                {
                    vm.NewsScrollUrl = string.Empty;
                }
                return View(vm);
            }
        }

        private string buildNextNewsScrollingURL(NewsScroll data)
        {
            int newStartRow = data.EndNum + 1;
            int newEndRow = newStartRow + 2;
            StringBuilder url = new StringBuilder();
            string hostname = ConfigurationManager.AppSettings["Host"].ToString();
            if (!hostname.EndsWith("/"))
            {
                hostname = hostname + "/";
            }
            url.Append(hostname);
            url.Append("Home/NewsStories?storyStartRow=");
            url.Append(newStartRow);
            url.Append("&storyEndRow=");
            url.Append(newEndRow);
            //url.Append("&productStartRow=1");
            //url.Append("&productEndRow=5");

            return url.ToString();

        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Helper method that adds the Identity cookie to the request output
        /// headers. Assigns the userState to the claims for holding user
        /// data without having to reload the data from disk on each request.
        /// 
        /// AppUserState is read in as part of the baseController class.
        /// </summary>
        /// <param name="appUserState"></param>
        /// <param name="providerKey"></param>
        /// <param name="isPersistent"></param>
        private void IdentitySignin(AppUserState appUserState, string providerKey = null)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.Email, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.Name, appUserState.Email));
            // serialized AppUserState object
            claims.Add(new Claim("userState", appUserState.ToString()));

            var identity = new ClaimsIdentity(claims, Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;

            // add to user here!
            int ExpireDays = Int32.Parse(ConfigurationManager.AppSettings["ExpireAuthenticationCookieInDays"]);
            var authProperties = new Microsoft.Owin.Security.AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            };

            AuthenticationManager.SignIn(authProperties, identity);
        }

    }
}