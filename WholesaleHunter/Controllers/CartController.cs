﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using WholesaleHunter.AuthorizeDotNet;
using WholesaleHunter.Models;
using WholesaleHunter.ViewModels;
using WholesaleHunter.Classes;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using UpsRateApi;

namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    public class CartController : Controller
    {
        public const int CREDIT_CARD_APPROVED = 1;
        public const int CREDIT_CARD_DECLINED = 2;
        public const int CREDIT_CARD_DECLINED_WITH_ERROR = 3;
        public const int CREDIT_CARD_HOLD_FOR_REVIEW = 4;

        public const int PAYMENT_METHOD_CREDITCARD = 1;
        public const int PAYMENT_METHOD_CHECK = 2;

        public const int ERROR_UNABLE_TO_COMPLETE_ORDER = -500;
        public const int ERROR_CREATE_NEW_USER = -600;

        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";

        [Authorize]
        public ActionResult CompletePayment(AlmostThere data)
        {
            try
            {
                int CustomerID = 0;
                string Email = "";
                string ConfirmationEmailHTML = "";
                string ConfirmationReceiptHTML = "";
                if (getClaims())
                {
                    CustomerID = this.appuser.UserID;
                    Email = this.appuser.Email;
                }
                else
                {
                    ErrorViewModel evm = GetErrorViewModel(-400);
                    return View("Error", evm);
                }


                //first verify the cart inventory
                int cartstatus;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", CustomerID);
                    cartstatus = db.Query<int>("WholesaleHunterCom.diuVerifyCartInventory", p, commandType: CommandType.StoredProcedure).Single<int>();
                    // 1 for no problems, 0 for general failure, -1 for quantity was reduced, -2 for some quantity was shifted to a higher price
                }

                if (cartstatus != 1)
                {
                    ErrorViewModel evm = GetErrorViewModel(cartstatus);
                    return View("Error", evm);
                }

                //we have to build the confirmation email now, because after the credit card is succesfully
                //processed the contents of the cart will be emptied
                CartViewModel cvm = getCartViewModel();
                if(cvm.shipping!=null) { 
                    if(cvm.shipping.ShippingCost == null)
                    {
                        ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                        return View("Error", evm);
                    }
                }

                CartPaymentViewModel cpvm = getCartPaymentViewModel(cvm);
                if(cpvm.ShippingCostIsNull)
                {
                    ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                    return View("Error", evm);
                }

                cpvm.AlmostThere = data;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    //get the userid
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", CustomerID);
                    cpvm.CartInfo = db.Query<CartMyInfo>("WholesaleHunterCom.sMyCartInfoAddresses", p, commandType: CommandType.StoredProcedure).First();
                }


                ConfirmationEmailHTML = ViewRenderer.RenderView("~/Views/Cart/ConfirmationEmail.cshtml", cpvm, ControllerContext);
                ConfirmationReceiptHTML = ViewRenderer.RenderView("~/Views/Cart/ConfirmationEmailCheck.cshtml", cpvm, ControllerContext);
                
                int OrderID;
                //do the initial save and get the OrderID
                string JsonAuthorizeNETToken = "";


                OrderID = GetOrderID(data, CustomerID, out JsonAuthorizeNETToken);


                if (OrderID < 1)
                {
                    //WholesaleHunterCom.iOrder returns a 0 if an error is encountered
                    ErrorViewModel evm = GetErrorViewModel(OrderID);
                    return View("Error", evm);
                }

                if (data.PaymentMethod == PAYMENT_METHOD_CREDITCARD)
                {
                    //this is a credit card payment
                    ChargeInformation chargeInfo = BuildChargeInformation(data, CustomerID);
                    chargeInfo.OrderID = OrderID;
                    chargeInfo.CustomerID = CustomerID;

                    CustomerAddress address = GetCustomerAddress(CustomerID, data);

                    ChargeStatus status;
                    //did the customer enter a new credit card or a saved one
                    if (data.SelectCard == 0)
                    {
                        status = ProcessNewCreditCardPayment(data, chargeInfo);
                    }
                    else
                    {
                        status = ProcessSavedCreditCardPayment(data, chargeInfo, JsonAuthorizeNETToken);
                    }

                    if (status.Status < 1)
                    {
                        ErrorViewModel evm = GetErrorViewModel(status.Status);
                        return View("Error", evm);
                    }

                    //show the order completion screen
                    CartCompleteViewModel vm = new CartCompleteViewModel();
                    if (status.Status==1)
                    {
                        vm.EmailSent = SendConfirmationEmail(ConfirmationEmailHTML, data.Email, OrderID);
                        vm.EmailAddress = data.Email;
                    }
                    else
                    {
                        vm.EmailSent = 0;
                        vm.EmailAddress = "";
                    }
                    vm.OrderNumber = OrderID;
                    vm.Status = status;
                    vm.ConfirmationEmailHTML = ConfirmationReceiptHTML;
                    return View("Complete", vm);
                }
                else
                {
                    //this is a check payment

                    //the results is the Order ID
                    //show the order completion screen
                    CartCompleteViewModel vm = new CartCompleteViewModel();
                    vm.OrderNumber = OrderID;
                    vm.PaidByCheck = true;
                    ChargeStatus status = new ChargeStatus();
                    status.Status = 0;
                    status.ErrorCode = "";
                    status.ErrorText = "";
                    vm.Status = status;
                    vm.EmailSent = SendConfirmationEmail(ConfirmationEmailHTML, data.Email, OrderID);
                    vm.EmailAddress = data.Email;
                    vm.ConfirmationEmailHTML = ConfirmationReceiptHTML;
                    return View("Complete", vm);
                }
            }
            catch (Exception exc)
            {
                ErrorViewModel evm = new ErrorViewModel();
                StringBuilder msg = new StringBuilder();
                msg.Append(exc.Message);
                msg.Append("<br />");
                msg.Append(exc.StackTrace);
                evm.Message = msg.ToString();
                return View("Error", evm);
            }
        }

        private ChargeInformation BuildChargeInformation(AlmostThere data, int CustomerID)
        {
            ChargeInformation chargeInfo = new ChargeInformation();

            if(data.SelectCard == 0) { 
                //billing to a new credit card
                StringBuilder ExpireDate = new StringBuilder();
                string month = "0" + data.ExpireMonth;
                ExpireDate.Append(month.Substring(month.Length - 2));
                ExpireDate.Append(data.ExpireYear.Substring(data.ExpireYear.Length - 2));
                chargeInfo.ExpirationDate = ExpireDate.ToString();
                chargeInfo.CardNumber = data.CardNumber;

                //billing address
                CustomerAddress BillTo = new CustomerAddress();
                BillTo.Company = data.BillingName;
                BillTo.CustomerID = CustomerID;
                BillTo.Address = data.Address;
                BillTo.City = data.City;
                BillTo.Email = data.Email;
                BillTo.FirstName = data.FirstName;
                BillTo.LastName = data.LastName;
                BillTo.PhoneNumber = data.BillingPhone;
                BillTo.State = data.State;
                BillTo.Zip = data.Zip;
                chargeInfo.BillTo = BillTo;
            }

            chargeInfo.TransactionAmount = data.OrderTotal;


            chargeInfo.CCV = data.CVV;
            if (data.RememberCard == 1)
            {
                chargeInfo.CreateProfile = true;
            }
            else
            {
                chargeInfo.CreateProfile = false;
            }
            chargeInfo.OrderDescription = data.OrderDescription;

            //shipping address
            CustomerAddress ShipTo = new CustomerAddress();
            ShipTo.FirstName = data.FirstName;
            ShipTo.LastName = data.LastName;
            ShipTo.Address = data.ShippingAddress1;
            ShipTo.City = data.ShippingCity;
            ShipTo.State = data.ShippingState;
            ShipTo.Zip = data.ShippingZip;
            ShipTo.Country = "USA";
            chargeInfo.ShipTo = ShipTo;

            return chargeInfo;
        }

        private int GetOrderID(AlmostThere data, int CustomerID, out string JsonAuthorizeNETToken)
        {
            JsonAuthorizeNETToken = "";
            string lastFour = "";
            if (data.PaymentMethod == 1)
            {
                //this is a credit card payment
                if (data.SelectCard == 0)
                {   //a new credit card was entered
                    lastFour = data.CardNumber.Substring(data.CardNumber.Length - 4);
                }
                else
                {
                    //charging to a saved credit card
                    lastFour = data.SelectedCardDescription.Substring(data.SelectedCardDescription.Length - 4);

                    List<MyPaymentMethod> listPaymentMethods;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                    {
                        var p = new DynamicParameters();
                        p.Add("@CustomerID", CustomerID);
                        listPaymentMethods = db.Query<MyPaymentMethod>("WholesaleHunterCom.sMyPaymentMethods", p, commandType: CommandType.StoredProcedure).ToList<MyPaymentMethod>();
                    }

                    foreach (MyPaymentMethod paymethod in listPaymentMethods)
                    {
                        if (paymethod.CustomerPaymentMethodID == data.SelectCard)
                        {
                            JsonAuthorizeNETToken = paymethod.AuthorizeNETToken;
                            DateTime expire = paymethod.ExpirationDate;
                            data.ExpireMonth = expire.Month.ToString();
                            data.ExpireYear = expire.Year.ToString().Substring(2,2);
                            break;
                        }
                    }
                }
            }

            decimal shippinginsurance = 0;
            if (data.ShippingInsurance != 0)
            {
                shippinginsurance = data.ShippingInsuranceCost;
            }
            decimal ShippingCost = data.ShippingCost + shippinginsurance + data.Hazmat;

            int paystatus;
            if(data.PaymentMethod == PAYMENT_METHOD_CREDITCARD)
            {
                //credit card payment, payment status will be set in UpdateOrder()
                paystatus = 0;
            }
            else
            {
                //check or money order payment
                paystatus = 3;
            }

            int OrderID;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", CustomerID);
                p.Add("@PaymentStatus", paystatus);
                p.Add("@ShippingCost", ShippingCost);
                p.Add("@Tax", data.Tax);
                p.Add("@ShippingInsurancePrice", shippinginsurance);
                p.Add("@LastFour", lastFour);

                if(data.PaymentMethod== PAYMENT_METHOD_CREDITCARD) { 
                    //credit card payment
                    p.Add("@BillName", data.BillingName);
                    p.Add("@BillPhone", data.BillingPhone);
                    p.Add("@BillingAddress", data.Address);
                    p.Add("@BillingCity", data.City);
                    p.Add("@BillingState", data.State);
                    p.Add("@BillingZIP", data.Zip);
                }
                else
                {
                    //check or money order payment
                    p.Add("@BillingAddress", data.ShippingAddress1);
                    p.Add("@BillingCity", data.ShippingCity);
                    p.Add("@BillingState", data.ShippingState);
                    p.Add("@BillingZIP", data.ShippingZip);
                    p.Add("@BillName", data.FirstName + " " + data.LastName);
                    p.Add("@BillPhone", data.PhoneNumber);
                }

                int CCExpireMonth;
                if (Int32.TryParse(data.ExpireMonth, out CCExpireMonth))
                {
                    p.Add("@CCExpMo", CCExpireMonth);
                }

                int CCExpireYear;
                if (Int32.TryParse(data.ExpireYear, out CCExpireYear))
                {
                    p.Add("@CCExpYear", CCExpireYear);
                }

                OrderID = db.Query<int>("WholesaleHunterCom.iOrder", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
                //OrderID==0 when an error occured
                return OrderID;
            }
        }

        private ChargeStatus ProcessNewCreditCardPayment(AlmostThere data, ChargeInformation chargeInfo)
        {
            //the customer entered a new credit card
            ChargeCreditCard charge = new ChargeCreditCard();
            charge.ProcessCharge(chargeInfo);

            ChargeStatus chargestatus = new ChargeStatus();

            if (charge.transactionResult == null)
            {
                chargestatus.Status = -200;
                return chargestatus;
            }

            int results = UpdateOrder(charge.transactionResult, charge.profileResult, data, chargeInfo.CustomerID, chargeInfo.OrderID);
            if (results < 1)
            {
                chargestatus.Status = - 100;
                return chargestatus;
            }

            switch (Int32.Parse(charge.transactionResult.ResponseCode))
            {
                case 1:
                    //approved
                    chargestatus.Status = CREDIT_CARD_APPROVED;
                    break;
                case 2:
                    //declined
                    chargestatus.Status = CREDIT_CARD_DECLINED;
                    break;
                case 3:
                    //declined with error
                    chargestatus.Status = CREDIT_CARD_DECLINED_WITH_ERROR;
                    if(charge.transactionResult.Errors.Count>0)
                    {
                        chargestatus.ErrorCode = charge.transactionResult.Errors[0].errorCode;
                        chargestatus.ErrorText = charge.transactionResult.Errors[0].errorText;
                    }
                    break;
                case 4:
                    //hold for review
                    chargestatus.Status = CREDIT_CARD_HOLD_FOR_REVIEW;
                    break;
            }

            return chargestatus;
        }

        private ChargeStatus ProcessSavedCreditCardPayment(AlmostThere data, ChargeInformation chargeInfo, string JsonAuthorizeNETToken)
        {
            //the customer wants to use a previously "saved" card.
            PaymentProfile prof = Newtonsoft.Json.JsonConvert.DeserializeObject<PaymentProfile>(JsonAuthorizeNETToken);
            ChargeCustomerProfile charge = new ChargeCustomerProfile();
            data.RememberCard = 0;
            data.MakeDefault = 0;

            ChargeStatus chargestatus = new ChargeStatus();

            TransactionResult tr = charge.ChargeProfile(prof.CustomerProfileID, prof.CustomerPaymentProfileID, chargeInfo);

            if (tr == null)
            {
                chargestatus.Status = -200;
                return chargestatus;
            }

            int results = UpdateOrder(tr, null, data, chargeInfo.CustomerID, chargeInfo.OrderID);

            if (results < 1)
            {
                chargestatus.Status = -100;
                return chargestatus;
            }


            switch (Int32.Parse(tr.ResponseCode))
            {
                case 1:
                    //approved
                    chargestatus.Status = CREDIT_CARD_APPROVED;
                    break;
                case 2:
                    //declined
                    chargestatus.Status = CREDIT_CARD_DECLINED;
                    if (tr.Errors.Count > 0)
                    {
                        chargestatus.ErrorCode = tr.Errors[0].errorCode;
                        chargestatus.ErrorText = tr.Errors[0].errorText;
                    }
                    break;
                case 3:
                    //declined with error
                    chargestatus.Status = CREDIT_CARD_DECLINED_WITH_ERROR;
                    break;
                case 4:
                    //hold for review
                    chargestatus.Status = CREDIT_CARD_HOLD_FOR_REVIEW;
                    break;
            }

            return chargestatus;
        }

        private ErrorViewModel GetErrorViewModel(int errortype)
        {
            ErrorViewModel evm = new ErrorViewModel();
            if (errortype == 0)
            {
                evm.Message = "An error was encountered processing your order. If this message persists please contact a System Administrator.";
                evm.Title = "Error Encountered";
                evm.ShowBackToPaymentButton = true;
            }
            else if (errortype == -1)
            {
                evm.Message = "Insufficient quantity, the order cannot be filled.";
                evm.Title = "Insufficient Quantity";
                evm.ShowBackToPaymentButton = true;
            }
            else if (errortype == -2)
            {
                evm.Message = "There were no items in this order.";
                evm.Title = "Cart Is Empty";
                evm.ShowBackToPaymentButton = false;
            }
            else if (errortype == -3)
            {
                evm.Message = "Your card was declined. Please check that your information was correct and try again.";
                evm.Title = "Card Declined";
                evm.ShowBackToPaymentButton = true;
            }
            else if (errortype == -100)
            {
                evm.Message = "Error encountered updating Order database.";
                evm.Title = "Error Encountered";
                evm.ShowOkButton = true;
            }
            else if (errortype == -200)
            {
                evm.Message = "Unspecified error processing credit card.";
                evm.Title = "Error Encountered";
                evm.ShowOkButton = true;
            }

            else if (errortype == -400)
            {
                evm.Message = "The user is not logged in.";
                evm.Title = "User Not Logged In";
                evm.ShowOkButton = true;
            }

            else if (errortype == ERROR_UNABLE_TO_COMPLETE_ORDER)
            {
                evm.Message = "We’re sorry.  We can’t complete your order at this time, please contact customer service at 888-900-HUNT (4868)";
                evm.Title = "Unable to complete order.";
                evm.ShowOkButton = true;
            }

            else if (errortype == ERROR_CREATE_NEW_USER)
            {
                evm.Message = "We’re sorry. An error was encountered trying to create a new user. If this error persists, please contact customer service at 888-900-HUNT (4868)";
                evm.Title = "Error";
                evm.ShowOkButton = true;
            }

            return evm;
        }


        private int SendConfirmationEmail(string html, string EmailAddress, int OrderNumber)
        {
            int status = 0;
            try { 
                html = html.Replace("###OrderNumber###", OrderNumber.ToString());
                string subject = ConfigurationManager.AppSettings["ConfirmationMailSubject"].ToString() + " " + OrderNumber.ToString();
                string from = ConfigurationManager.AppSettings["ConfirmationMailFrom"].ToString();
                SMTPEmail email = new SMTPEmail();
                email.Compose(subject, new MailAddress(EmailAddress), new MailAddress(from), html, true);
                email.Send();
            }
            catch
            {
                status = -1;
            }
            return status;
        }

        private int UpdateOrder(TransactionResult tr, CustomerProfileResult pr, AlmostThere data, int CustomerID, int OrderID)
        {
            int results;
            //first get the PaymentStatus
            int PaymentStatus = 0;
            if (tr.ResponseCode == null)
            {
                return 0;
            }

            switch(Int32.Parse(tr.ResponseCode))
            {
                case 1:
                    //approved
                    PaymentStatus = 3;
                    break;
                case 2:
                    //declined
                    PaymentStatus = 2;
                    break;
                case 3:
                    //declined with error
                    PaymentStatus = 2;
                    break;
                case 4:
                    //hold for review
                    PaymentStatus = 35;
                    break;
            }

            string jsonProfile = "";

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@OrderID", OrderID);
                p.Add("@PaymentStatus", PaymentStatus);
                p.Add("@CCTransID", tr.TransactionID);
                p.Add("@CCAuthCode", tr.AuthCode);
                p.Add("@CCAuthMessage", tr.GetMessages());
                if ((data.RememberCard == 1) && (pr != null))
                {
                    PaymentProfile prof = new PaymentProfile();
                    prof.CustomerProfileID = pr.CustomerProfileId;
                    foreach (string CustomerPaymentProfile in pr.CustomerPaymentProfileList)
                    {
                        prof.CustomerPaymentProfileID = CustomerPaymentProfile;
                        break;
                    }

                    if((prof.CustomerPaymentProfileID == null) || (prof.CustomerProfileID==null))
                    {
                        //the customer payment profile data is not present
                        p.Add("@AuthorizeNETToken", "");
                        data.RememberCard = 0;
                    }
                    else
                    {
                        jsonProfile = Newtonsoft.Json.JsonConvert.SerializeObject(prof);
                        p.Add("@AuthorizeNETToken", jsonProfile);
                    }
                }
                else
                {
                    p.Add("@AuthorizeNETToken", "");
                    data.RememberCard = 0;
                }
                p.Add("@CCAvsResponse", tr.CVVResultCode);
                p.Add("@vsResult", tr.ResponseCode);
                p.Add("@vsErrorCodes", tr.ErrorCode);
                p.Add("@vsAVSAddr", tr.AVSResultCode);
                results = db.Query<int>("WholesaleHunterCom.uOrderPostCard", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
            }

            //see if we need to save the payment method
            if (data.RememberCard == 1)
            {
                StringBuilder ExpireDate = new StringBuilder();
                ExpireDate.Append("20");
                ExpireDate.Append(data.ExpireYear);
                ExpireDate.Append("-");
                ExpireDate.Append(data.ExpireMonth);
                ExpireDate.Append("-");

                switch (Int32.Parse(data.ExpireMonth))
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        {
                            ExpireDate.Append("31");
                            break;
                        }
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        {
                            ExpireDate.Append("30");
                            break;
                        }
                    default:
                        {
                            ExpireDate.Append("28");
                            break;
                        }
                }

                string lastFour = "";
                //this is a credit card payment
                if (data.SelectCard == 0)
                {   //a new credit card was entered
                    lastFour = data.CardNumber.Substring(data.CardNumber.Length - 4);
                }
                else
                {
                    //charging to a saved credit card
                    lastFour = data.SelectedCardDescription.Substring(data.SelectedCardDescription.Length - 4);
                }

                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", CustomerID);
                    p.Add("@LastFour", lastFour);
                    p.Add("@AuthorizeNETToken", jsonProfile);
                    p.Add("@ExpirationDate", ExpireDate.ToString());
                    p.Add("@MakePrimary", data.MakeDefault);
                    p.Add("@BillAddr1", data.Address);
                    p.Add("@BillCity", data.City);
                    p.Add("@BillState", data.State);
                    p.Add("@BillZIP", data.Zip);
                    p.Add("@BillName", data.BillingName);
                    p.Add("@BillPhone", data.BillingPhone);
                    int status = db.Query<int>("WholesaleHunterCom.iMyPaymentMethod", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
                }
            }
            return results;
        }

        private CustomerAddress GetCustomerAddress(int CustomerID, AlmostThere data)
        {
            CustomerAddress addr = new CustomerAddress();
            addr.CustomerID = CustomerID;
            addr.Address = data.Address;
            addr.City = data.City;
            addr.Email = data.Email;
            addr.FirstName = data.FirstName;
            addr.LastName = data.LastName;
            addr.PhoneNumber = data.PhoneNumber;
            addr.State = data.State;
            addr.Zip = data.CreditCardZIP;
            return addr;
        }

        [Authorize]
        public ActionResult AlmostThere(AlmostThere data)
        {
            CartViewModel cvm = getCartViewModel();
            if (cvm.shipping != null)
            {
                if (cvm.shipping.ShippingCost == null)
                {
                    ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                    return View("Error", evm);
                }
            }

            CartPaymentViewModel vm = getCartPaymentViewModel(cvm);
            if (vm.ShippingCostIsNull)
            {
                ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                return View("Error", evm);
            }

            vm.AlmostThere = data;

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.CartInfo = db.Query<CartMyInfo>("WholesaleHunterCom.sMyCartInfoAddresses", p, commandType: CommandType.StoredProcedure).First();
            }
            if(vm.CartInfo.FFLID==null)
            {
                vm.CartInfo.FFLID = string.Empty;
            }
            return View("AlmostThere", vm);
        }

        [Authorize]
        public ActionResult Payment()
        {
            CartViewModel cvm = getCartViewModel();
            if (cvm.shipping != null)
            {
                if (cvm.shipping.ShippingCost == null)
                {
                    ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                    return View("Error", evm);
                }
            }


            if (cvm.CartIsEmpty)
            {
                ErrorViewModel evm = new ErrorViewModel();
                evm.Title = "Cart Is Empty";
                evm.Message = "Your cart is now empty.";
                return View("Error", evm);
            }
            CartPaymentViewModel vm = getCartPaymentViewModel(cvm);
            if (vm.ShippingCostIsNull)
            {
                ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                return View("Error", evm);
            }

            if (vm.HazmatAmount == -1)
            {
                ErrorViewModel evm = new ErrorViewModel();
                evm.Title = "HAZMAT Violation";
                evm.Message = "Due to HAZMAT restrictions certain items, such as powder or ammunition,\ncannot be shipped to your ZIP Code.";
                evm.ShowBackToMyInfoButton = true;
                return View("Error", evm);
            }

            //now get shipping cost estimate for any UPS shipping methods
            GetUPSRates(ref vm);

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {

                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                var results = db.QueryMultiple("WholesaleHunterCom.sMyCartInfo", p, commandType: CommandType.StoredProcedure);

                vm.CartInfo = results.Read<CartMyInfo>().Single();
                List<CartMyAddress> Addresses = results.Read<CartMyAddress>().ToList(); //not used
                int FFLRequired = results.Read<int>().Single(); //not used
            }

            return View("Payment", vm);
        }

        private void GetUPSRates(ref CartPaymentViewModel vm)
        {
            //make a unique list of shipping methods
            Dictionary<string, CartShippingMethod> uniquemethods = new Dictionary<string, CartShippingMethod>();
            foreach (CartShippingMethod shipmethod in vm.ShippingMethods)
            {
                if (shipmethod.ShippingMethodName.ToLower().StartsWith("ups"))
                {
                    if(!uniquemethods.ContainsKey(shipmethod.ShippingMethodName))
                    uniquemethods.Add(shipmethod.ShippingMethodName, shipmethod);
                }
            }

            foreach(KeyValuePair<string, CartShippingMethod> pair in uniquemethods)
            {
                CartShippingMethod shipmethod = pair.Value;
                UPS_API ups = new UPS_API();
                ups.Username = ConfigurationManager.AppSettings["UpsUserId"];
                ups.Password = ConfigurationManager.AppSettings["UpsPassword"];
                ups.AccessLicense = ConfigurationManager.AppSettings["UpsAccessLicense"];
                ups.CustomerID = vm.CustomerID.ToString();

                ShipAddress Shipper = new ShipAddress();
                Shipper.Name = ConfigurationManager.AppSettings["UpsShipperName"];
                Shipper.ID = ConfigurationManager.AppSettings["UpsShipperID"];
                Shipper.AddressLine = ConfigurationManager.AppSettings["UpsShipperAddressLine"];
                Shipper.City = ConfigurationManager.AppSettings["UpsShipperCity"];
                Shipper.State = ConfigurationManager.AppSettings["UpsShipperState"];
                Shipper.Zip = ConfigurationManager.AppSettings["UpsShipperZip"];
                Shipper.Country = "US";
                ups.Shipper = Shipper;

                ShipAddress ShipTo = new ShipAddress();
                ShipTo.AddressLine = shipmethod.ToAddress;
                ShipTo.City = shipmethod.ShipCity;
                ShipTo.State = shipmethod.ShipState;
                ShipTo.Zip = shipmethod.ToZIP;
                ShipTo.Country = "US";
                ups.ShipTo = ShipTo;

                ShipAddress ShipFrom = new ShipAddress();
                ShipFrom.AddressLine = shipmethod.FromAddress;
                ShipFrom.City = shipmethod.FromCity;
                ShipFrom.State = shipmethod.FromState;
                ShipFrom.Zip = shipmethod.FromZIP;
                ShipFrom.Country = "US";
                ups.ShipFrom = ShipFrom;

                ups.NegotiatedRatesIndicator = "";

                ups.Weight = string.Format("{0:N2}", shipmethod.Weight);

                //get the UPS Service Code from ShippingMethodName
                //there are 3 UPS Service Codes Ship.dbo.ShippingMethod
                // ID   - MethodName          - UPS Service Code
                // 1    - UPS Ground           - 03
                // 2    - UPS Second Day Air   - 02
                // 7    - UPS SurePost         - 93
                if (shipmethod.ShippingMethodName.ToLower().Contains("second"))
                {
                    ups.ServiceCode = "02";
                    ups.ServiceDescription = "UPS 2nd Day Air";
                }
                else if (shipmethod.ShippingMethodName.ToLower().Contains("surepost"))
                {
                    ups.ServiceCode = "93";
                    ups.ServiceDescription = "UPS SurePost";
                }
                else 
                {
                    //defaults to ground
                    ups.ServiceCode = "03";
                    ups.ServiceDescription = "UPS Ground";
                }

                RateResponse response = ups.GetRateResponse();
                if(response.ResponseType.ToLower()=="rateresponse")
                {
                    int ResponseStatusCode;
                    if(Int32.TryParse(response.ResponseStatusCode, out ResponseStatusCode))
                    {
                        if(ResponseStatusCode==1)
                        {
                            //now put the price back in vm.ShippingMethods
                            foreach (CartShippingMethod method in vm.ShippingMethods)
                            {
                                if (method.ShippingMethodName == shipmethod.ShippingMethodName)
                                {
                                    method.ShippingCost = String.Format("{0:N2}", response.TotalCharges);
                                }
                            }
                        }
                    }
                }
            }
        }


        [HttpPost]
        [Authorize]
        public ActionResult PaymentWithFFL(CartFFL ffl)
        {
            CartViewModel cvm = getCartViewModel();
            if (cvm.shipping != null)
            {
                if (cvm.shipping.ShippingCost == null)
                {
                    ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                    return View("Error", evm);
                }
            }

            if (cvm.CartIsEmpty)
            {
                ErrorViewModel evm = new ErrorViewModel();
                evm.Title = "Cart Is Empty";
                evm.Message = "Your cart is now empty.";
                return View("Error", evm);
            }
            CartPaymentViewModel vm = getCartPaymentViewModel(cvm);
            if (vm.ShippingCostIsNull)
            {
                ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                return View("Error", evm);
            }

            if (vm.HazmatAmount == -1)
            {
                ErrorViewModel evm = new ErrorViewModel();
                evm.Title = "HAZMAT Violation";
                evm.Message = "Due to HAZMAT restrictions certain items, such as powder or ammunition,\ncannot be shipped to the ZIP Code where the selected FFL is located.";
                evm.ShowBackToMyInfoButton = true;
                return View("Error", evm);
            }


            if (ffl.FFLName == null) ffl.FFLName = string.Empty;
            if (ffl.ContactName == null) ffl.ContactName = "";
            if (ffl.Email == null) ffl.Email = string.Empty;
            if (ffl.Phone == null) ffl.Phone = string.Empty;
            if (ffl.Address1 == null) ffl.Address1 = string.Empty;
            if (ffl.City == null) ffl.City = string.Empty;
            if (ffl.State == null) ffl.State = string.Empty;
            if (ffl.ZIPCode == null) ffl.ZIPCode = string.Empty;
            if (ffl.FFLNumber == null) ffl.FFLNumber = string.Empty;

            int results;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                p.Add("@FFLID", ffl.FFLID);
                p.Add("@FFLName", ffl.FFLName);
                p.Add("@ContactName", ffl.ContactName);
                p.Add("@Email", ffl.Email);
                p.Add("@Phone", ffl.Phone);
                p.Add("@Address1", ffl.Address1);
                p.Add("@City", ffl.City);
                p.Add("@State", ffl.State);
                p.Add("@ZIPCode", ffl.ZIPCode);
                p.Add("@FFLNumber", ffl.FFLNumber);
                results = db.Query<int>("WholesaleHunterCom.uFFLChoice", p, commandType: CommandType.StoredProcedure).Single<int>();
            }

            vm.FFLStatus = results;
            if (results != 1)
            {
                //problem was encountered
                vm.Message = "An error occurred processing the FFL";
            }
            else
            {
                vm.Message = "The FFL was successfully processed";
            }

            GetUPSRates(ref vm);

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {

                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                var multi = db.QueryMultiple("WholesaleHunterCom.sMyCartInfo", p, commandType: CommandType.StoredProcedure);

                vm.CartInfo = multi.Read<CartMyInfo>().Single();
                List<CartMyAddress> Addresses = multi.Read<CartMyAddress>().ToList(); //not used
                int FFLRequired = multi.Read<int>().Single(); //not used
            }

            return View("Payment", vm);
        }

        private CartPaymentViewModel getCartPaymentViewModel(CartViewModel cvm)
        {
            CartPaymentViewModel vm = new ViewModels.CartPaymentViewModel(cvm);
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
                vm.IsRegistered = true;
            }
            else
            {
                vm.CustomerID = 0;
                vm.Email = "";
                vm.IsRegistered = false;
            }

            //verify the cart inventory levels
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CUstomerID", this.appuser.UserID);
                vm.CartInventoryCheck = db.Query<int>("WholesaleHunterCom.diuVerifyCartInventory", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
            }

            switch (vm.CartInventoryCheck)
            {
                case 0:
                    //general failutre
                    vm.CartAdjustedMessage = "[*BOM]We're sorry but an error has occurred while checking your cart contents.\n\nIf this message persists please contact our Customer Service department.[*EOM]";
                    break;
                case -1:
                    //quantity was reduced
                    vm.CartAdjustedMessage = "[*BOM]We're sorry.\n\nSome items in your cart have sold out at the selected price level.\n\nWe had to make some adjustments for you.\n\nPlease verify wether these changes are acceptable.[*EOM]";
                    break;
                case -2:
                    //some quantity shifted to a higher price
                    vm.CartAdjustedMessage = "[*BOM]We're sorry.\n\nSome items in your cart have sold out at the selected price level.\n\nWe had to make some adjustments for you.\n\nPlease verify wether these changes are acceptable.[*EOM]";
                    break;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CUstomerID", this.appuser.UserID);
                var results = db.QueryMultiple("WholesaleHunterCom.sCartPayment", p, commandType: CommandType.StoredProcedure);

                vm.items = results.Read<CartListItem>().ToList();
                CartShipping cartshipinfo = results.Read<CartShipping>().First();
                if (cartshipinfo.Tax == null && cartshipinfo.Hazmat == -1)
                {
                    vm.Tax = -1;
                    vm.HazmatAmount = -1;
                }
                else
                {
                    vm.Tax = (decimal)cartshipinfo.Tax;
                }
                vm.ShippingMethods = results.Read<CartShippingMethod>().ToList();
                foreach(CartShippingMethod method in vm.ShippingMethods)
                {
                    if(method.ShippingCost == null)
                    {
                        vm.ShippingCostIsNull = true;
                        break;
                    }
                }
                vm.PaymentMethods = results.Read<CartPaymentMethod>().ToList();
                vm.ShippingInsurancePrice = results.Read<decimal>().First();
            }

            vm.Tax = decimal.Round(vm.Tax, 2);

            NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            vm.TaxDisplay = String.Format(nfi, "{0:C2}", vm.Tax);

            return vm;
        }


        [HttpPost]
        public ActionResult SubmitMyInfo(CartMyInfoSubmit info)
        {
            MyInfoViewModel vm = new MyInfoViewModel();
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
            }
            else
            {
                vm.CustomerID = 0;
                vm.Email = "";
            }

            int CurrentCustomerID = this.appuser.UserID;

            if (info.Address2 == null)
            {
                info.Address2 = "";
            }

            if (info.IsRegistered)
            {
                //the user is already registered

            }
            else if (info.IsNewUser)
            {
                if (info.Password != null && info.Password != string.Empty)
                {
                    //the user supplied a password so we need to create an account for them
                    int NewLoginStatus;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                    {
                        var p = new DynamicParameters();
                        p.Add("@Username", info.Email);
                        p.Add("@Password", info.Password);
                        p.Add("@FirstName", info.FirstName);
                        p.Add("@LastName", info.LastName);
                        p.Add("@Address1", info.Address1);
                        p.Add("@Address2", info.Address2);
                        p.Add("@City", info.City);
                        p.Add("@State", info.State);
                        p.Add("@ZIPCode", info.ZIPCode);
                        p.Add("@Phone", info.Phone);
                        NewLoginStatus = db.Query<int>("WholesaleHunterCom.iLoginCreate", p, commandType: CommandType.StoredProcedure).Single<int>();
                    }

                    if (NewLoginStatus == 0)
                    {
                        //An error occurred
                    }
                    else if (NewLoginStatus == -1)
                    {
                        //a user is already registered with that email address
                    }
                    else
                    {
                        //sign the user in
                        AppUserState appUserState = new AppUserState();
                        appUserState.Email = info.Email;
                        appUserState.UserID = NewLoginStatus;

                        IdentitySignin(appUserState, "providerKey");

                        int status;
                        //now we need to transfer the cart contents from the old user to the new user
                        using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                        {
                            var p = new DynamicParameters();
                            p.Add("@OldCustomerID", vm.CustomerID);
                            p.Add("@NewCustomerID", NewLoginStatus);
                            status = db.Query<int>("WholesaleHunterCom.uCartOwnership", p, commandType: CommandType.StoredProcedure).Single<int>();
                            if(status==0)
                            {
                                ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                                return View("Error", evm);
                            }
                        }
                    }
                }
                else
                {
                    //they didn't enter a password so just treat them as a guest
                    info.IsNewUser = false;
                    info.IsGuest = true;
                }
            }

            if (info.IsGuest)
            {
                //do something... maybe
            }

            int results;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                p.Add("@FirstName", info.FirstName);
                p.Add("@LastName", info.LastName);
                p.Add("@Email", info.Email);
                p.Add("@Phone", info.Phone);
                p.Add("@Address1", info.Address1);
                p.Add("@Address2", info.Address2);
                p.Add("@City", info.City);
                p.Add("@State", info.State);
                p.Add("@ZIPCode", info.ZIPCode);
                results = db.Query<int>("WholesaleHunterCom.iuMyCartInfo", p, commandType: CommandType.StoredProcedure).Single<int>();
            }

            if (results == 0)
            {
                return View("Error");
            }

            if (info.FFLRequired == 1)
            {
                CartFFLViewModel vmFFL = new CartFFLViewModel();
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", vm.CustomerID);
                    p.Add("@ZIPCode", info.ZIPCode);
                    //vmFFL.listFFL = db.Query<CartFFL>("WholesaleHunterCom.sMyFFLList", p, commandType: CommandType.StoredProcedure).ToList();
                    var multi = db.QueryMultiple("WholesaleHunterCom.sMyFFLList", p, commandType: CommandType.StoredProcedure);
                    vmFFL.listFFL = multi.Read<CartFFL>().ToList();
                    vmFFL.PrimaryFFL = multi.Read<CartPrimaryFFL>().FirstOrDefault();
                    if (vmFFL.PrimaryFFL == null)
                    {
                        vmFFL.PrimaryFFL = new CartPrimaryFFL();
                    }
                }

                foreach (CartFFL ffl in vmFFL.listFFL)
                {
                    ffl.Phone = formatPhone(ffl.Phone);
                }

                if (vmFFL.PrimaryFFL.Phone != string.Empty)
                {
                    vmFFL.PrimaryFFL.Phone = formatPhone(vmFFL.PrimaryFFL.Phone);
                }

                vmFFL.JSONFFL = Newtonsoft.Json.JsonConvert.SerializeObject(vmFFL.listFFL);
                return View("FFL", vmFFL);
            }
            else
            {
                return RedirectToAction("Payment");
            }
        }

        private string formatPhone(string phone)
        {
            StringBuilder output = new StringBuilder();
            if (phone.Length >= 10)
            {
                output.Append("(");
                output.Append(phone.Substring(0, 3));
                output.Append(") ");
                output.Append(phone.Substring(3, 3));
                output.Append("-");
                output.Append(phone.Substring(6));
            }
            else
            {
                output.Append(phone);
            }
            return output.ToString();
        }

        public ActionResult MyInfoNew()
        {
            MyInfoViewModel vm = new MyInfoViewModel();

            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
            }

            GetCartInfo(ref vm);

            //this is not a registered user
            vm.MyInfo.FirstName = "";
            vm.MyInfo.LastName = "";
            vm.MyInfo.Email = "";
            vm.MyInfo.Phone = "";
            vm.MyInfo.Address1 = "";
            vm.MyInfo.Address2 = "";
            vm.MyInfo.City = "";
            vm.MyInfo.State = "";
            vm.MyInfo.ZIPCode = "";
            vm.IsRegistered = "false";
            vm.IsGuest = "false";
            vm.IsNewUser = "true";

            // below is for testing, obviously
            /*
            vm.MyInfo.FirstName = "Test";
            vm.MyInfo.LastName = "User";
            vm.MyInfo.Email = "tuser@test.com";
            vm.MyInfo.Phone = "tuser@test.com";
            vm.MyInfo.Address1 = "1060 W Addison St";
            vm.MyInfo.Address2 = "";
            vm.MyInfo.City = "Chicago";
            vm.MyInfo.State = "IL";
            vm.MyInfo.ZIPCode = "60613";
            */

            return View("MyInfo", vm);
        }

        public ActionResult MyInfoGuest()
        {
            MyInfoViewModel vm = new MyInfoViewModel();

            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
            }

            GetCartInfo(ref vm);

            //this is not a registered user
            vm.MyInfo.FirstName = "";
            vm.MyInfo.LastName = "";
            vm.MyInfo.Email = "";
            vm.MyInfo.Phone = "";
            vm.MyInfo.Address1 = "";
            vm.MyInfo.Address2 = "";
            vm.MyInfo.City = "";
            vm.MyInfo.State = "";
            vm.MyInfo.ZIPCode = "";
            vm.IsRegistered = "false";
            vm.IsNewUser = "false";
            vm.IsGuest = "true";

            /* below is for testing, obviously
            //this is not a registered user 
            vm.MyInfo.FirstName = "Test";
            vm.MyInfo.LastName = "User";
            vm.MyInfo.Email = "tuser@test.com";
            vm.MyInfo.Phone = "tuser@test.com";
            vm.MyInfo.Address1 = "1060 W Addison St";
            vm.MyInfo.Address2 = "";
            vm.MyInfo.City = "Chicago";
            vm.MyInfo.State = "IL";
            vm.MyInfo.ZIPCode = "60613";
            vm.IsRegistered = false;
            vm.IsNewUser = false;
            vm.IsGuest = true; */


            return View("MyInfo", vm);
        }

        public ActionResult MyInfo()
        {
            //need to see if the user is registered, that is if they have an email address on file
            MyInfoViewModel vm = new MyInfoViewModel();
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
            }

            if (vm.Email == "")
            {
                vm.IsRegistered = "false";
                vm.IsGuest = "false";
                vm.IsNewUser = "true";
            }
            else
            {
                vm.IsRegistered = "true";
                vm.IsGuest = "false";
                vm.IsNewUser = "false";
            }
            GetCartInfo(ref vm);

            return View(vm);
        }

        private void GetCartInfo(ref MyInfoViewModel vm)
        {
            //this is a registered user
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                var results = db.QueryMultiple("WholesaleHunterCom.sMyCartInfo", p, commandType: CommandType.StoredProcedure);

                vm.MyInfo = results.Read<CartMyInfo>().Single();
                vm.Addresses = results.Read<CartMyAddress>().ToList();
                vm.JSONAddresses = Newtonsoft.Json.JsonConvert.SerializeObject(vm.Addresses);
                vm.FFLRequired = results.Read<int>().Single();
            }
        }

        public ActionResult MyInfoLogin()
        {
            //need to see if the user is registered, that is if they have an email address on file
            MyInfoViewModel vm = new MyInfoViewModel();
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
                vm.Email = this.appuser.Email;
            }
            else
            {
                vm.CustomerID = 0;
                vm.Email = "";
            }

            if (vm.Email == string.Empty)
            {
                return View(vm);
            }
            else
            {
                return RedirectToAction("MyInfo");
            }
        }


        // GET: Cart
        public ActionResult Contents()
        {
            CartViewModel vm = new CartViewModel();
            vm = getCartViewModel();
            if (vm.shipping != null)
            {
                if (vm.shipping.ShippingCost == null)
                {
                    ErrorViewModel evm = GetErrorViewModel(ERROR_UNABLE_TO_COMPLETE_ORDER);
                    return View("Error", evm);
                }
            }

            if (vm.CartIsEmpty)
            {
                ErrorViewModel evm = new ErrorViewModel();
                evm.Title = "Cart Is Empty";
                evm.Message = "Your cart is now empty.";
                return View("Error", evm);
            }
            return View(vm);
        }

        private CartViewModel getCartViewModel()
        {
            NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();

            CartViewModel vm = new CartViewModel();
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
            }
            else
            {
                vm.CustomerID = 0;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                var results = db.QueryMultiple("WholesaleHunterCom.sShoppingCart", p, commandType: CommandType.StoredProcedure);

                vm.items = results.Read<CartListItem>().ToList();
                if (vm.items.Count == 0)
                {
                    //the cart is empty
                    vm.CartIsEmpty = true;
                    return vm;
                }
                vm.CartIsEmpty = false;
                vm.shipping = results.Read<CartShipping>().Single();
                vm.suggested = results.Read<CartSuggestedProducts>().ToList();
                decimal shippingAmount;
                if (Decimal.TryParse(vm.shipping.ShippingCost, out shippingAmount))
                {
                    //shipping cost is a decimal value
                    vm.ShippingAmount = decimal.Round(shippingAmount, 2);
                    vm.ShippingDisplay = String.Format(nfi, "{0:C2}", vm.ShippingAmount);
                }
                else
                {
                    //shipping cost is NOT a decimal value
                    vm.ShippingDisplay = vm.shipping.ShippingCost;
                    vm.ShippingAmount = 0;
                }

                if (vm.shipping.Tax == 0 || vm.shipping.Tax == null)
                {
                    vm.TaxAmount = 0;
                    vm.TaxDisplay = "(unknown)";
                }
                else
                {
                    vm.TaxAmount = decimal.Round((decimal)vm.shipping.Tax, 2);
                    vm.TaxDisplay = String.Format(nfi, "{0:C2}", vm.TaxAmount);
                }

                vm.HazmatAmount = decimal.Round(vm.shipping.Hazmat, 2);
                vm.HazmatDisplay = String.Format(nfi, "{0:C2}", vm.HazmatAmount);

                if (vm.ShippingAmount == -2)
                {
                    vm.AllowedToShip = false;
                    vm.ShippingDisplay = "CANNOT SHIP";
                }
            }

            return vm;
        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }

        public void IdentitySignin(AppUserState appUserState, string providerKey = null)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.Email, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.Name, appUserState.Email));
            // serialized AppUserState object
            claims.Add(new Claim("userState", appUserState.ToString()));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;

            // add to user here!
            int ExpireDays = Int32.Parse(ConfigurationManager.AppSettings["ExpireAuthenticationCookieInDays"]);
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            };

            AuthenticationManager.SignIn(authProperties, identity);
        }
    }
}