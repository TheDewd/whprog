﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using WholesaleHunter.ViewModels;
using WholesaleHunter.Models;
using WholesaleHunter.Classes;
using System.Security.Claims;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    public class SearchController : Controller
    {
        private string dbname = "WholesaleHunterCom";

        public Dictionary<string, int> Categories;
        public AppUserState appuser;

        //search parameters
        public int CategoryID = 0;
        public decimal MinPrice = 0;
        public decimal MaxPrice = 10000;
        public int BrandID = 0;
        public int InStockOnly = 0;
        public int NewOnly = 0;
        public string Keywords = "";
        public string SortBy = "";
        public int StartRow = 1;
        public int EndRow = 500;

        public SearchController()
        {
            this.Categories = new Dictionary<string, int>();
            this.Categories.Add("guns", 106);
            this.Categories.Add("ammo", 10);
            this.Categories.Add("reloading", 216);
            this.Categories.Add("scopes", 197);
            this.Categories.Add("hunting", 50);
            this.Categories.Add("fishing", 618);
            this.Categories.Add("clearance", 106);
            this.Categories.Add("all categories", 106);
        }

        [HttpGet]
        public ActionResult Last()
        {
            int IsNewCustomer;
            int CustomerID;
            SearchViewModel vm;
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
            }

            SearchCriteria criteria;
            HttpCookie searchCookie = Request.Cookies["search"];
            if (searchCookie != null)
            {
                criteria = JsonConvert.DeserializeObject<SearchCriteria>(searchCookie.Value);
            }
            else
            {
                //create a default criteria
                criteria = new SearchCriteria();
                criteria.CategoryID = 0;
                criteria.MinPrice = 0m;
                criteria.MaxPrice = 0m;
                criteria.BrandID = 0;
                criteria.InStockOnly = false;
                criteria.NewOnly = false;
                criteria.Keywords = "";
                criteria.SortBy = "Popularity";
                criteria.StartRow = 1;
                criteria.EndRow = 15;
                criteria.filtersselected = "";
            }

            vm = getViewModel(criteria, IsNewCustomer);
            vm.SortBy = criteria.SortBy;
            vm.KeyWords = criteria.Keywords;
            vm.CategorySelected = criteria.CategoryID.ToString();
            vm.BrandSelected = criteria.BrandID.ToString();

            vm.InStockSelected = criteria.InStockOnly;
            vm.NewItemsSelected = criteria.NewOnly;

            vm.MinPrice = criteria.MinPrice;
            vm.MaxPrice = criteria.MaxPrice;
            vm.StartRow = criteria.StartRow;
            vm.EndRow = criteria.EndRow;
            vm.FiltersJSON = "";

            vm.ScrollUrl = buildNextScrollingURL(criteria);

            return View("Index", vm);
        }

        public ActionResult Scroll(SearchCriteria data)
        {
            StringBuilder output = new StringBuilder();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                ScrollViewModel vm = new ScrollViewModel();
                var p = new DynamicParameters();

                if (data.CategoryID == 0)
                {
                    p.Add("@CategoryID", (int?)null);
                }
                else
                {
                    p.Add("@CategoryID", data.CategoryID);
                }

                if (data.MinPrice == 0)
                {
                    p.Add("@MinPrice", (int?)null);
                }
                else
                {
                    p.Add("@MinPrice", data.MinPrice);
                }

                if (data.MaxPrice == 0)
                {
                    p.Add("@MaxPrice", (int?)null);
                }
                else
                {
                    p.Add("@MaxPrice", data.MaxPrice);
                }

                if (data.BrandID <= 0)
                {
                    p.Add("@BrandID", (int?)null);
                }
                else
                {
                    p.Add("@BrandID", data.BrandID);
                }

                p.Add("@InStockOnly", data.InStockOnly);
                p.Add("@NewOnly", data.NewOnly);

                if (data.Keywords == string.Empty)
                {
                    p.Add("@Keywords", null);
                    vm.Keywords = "";
                }
                else
                {
                    p.Add("@Keywords", data.Keywords);
                    vm.Keywords = data.Keywords;
                }

                p.Add("@SortBy", data.SortBy);
                p.Add("@StartRow", data.StartRow);
                p.Add("@EndRow", data.EndRow);
                if (data.filtersselected == null)
                {
                    p.Add("@ExtraFilters", null);
                }
                else
                {
                    p.Add("@ExtraFilters", data.filtersselected);
                }

                p.Add("@CustomerID", data.CustomerID);

                if (data.Dept != string.Empty)
                {
                    p.Add("@Dept", data.Dept);
                }

                vm.Products = db.Query<SearchProduct>("WholesaleHunterCom.sProductSearchResultsScrolling", p, commandType: CommandType.StoredProcedure).ToList<SearchProduct>();
                vm.NextURL = buildNextScrollingURL(data);
                return View(vm);
            }
        }

        [HttpGet]
        public ActionResult Keyword(string keywords)
        {
            int IsNewCustomer;
            int CustomerID;
            SearchViewModel vm;
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
            }

            //get rid of any leading or trailing spaces
            keywords = keywords.Trim();
            //now make sure there is only a single space between each word
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            keywords = regex.Replace(keywords, " ");

            SearchCriteria criteria = new SearchCriteria();
            criteria.CategoryID = 0;
            criteria.MinPrice = 0m;
            criteria.MaxPrice = 0m;
            criteria.BrandID = 0;
            criteria.InStockOnly = false;
            criteria.NewOnly = false;
            criteria.Keywords = keywords;
            criteria.SortBy = "Popularity";
            criteria.StartRow = 1;
            criteria.EndRow = 15;
            criteria.filtersselected = "";

            vm = getViewModel(criteria, IsNewCustomer);
            vm.SortBy = criteria.SortBy;
            vm.KeyWords = criteria.Keywords;
            vm.CategorySelected = criteria.CategoryID.ToString();
            vm.BrandSelected = criteria.BrandID.ToString();

            vm.InStockSelected = criteria.InStockOnly;
            vm.NewItemsSelected = criteria.NewOnly;

            vm.MinPrice = criteria.MinPrice;
            vm.MaxPrice = criteria.MaxPrice;
            vm.StartRow = criteria.StartRow;
            vm.EndRow = criteria.EndRow;
            vm.FiltersJSON = "";

            vm.ScrollUrl = buildNextScrollingURL(criteria);

            SetLastSearchCookie(criteria);
            return View("Index", vm);
        }

        private void SetLastSearchCookie(SearchCriteria criteria)
        {
            //now see if we can save criteria in cookie
            //string obj = JsonConvert.SerializeObject(criteria);
            dynamic obj = new JObject();
            obj.CategoryID = criteria.CategoryID;
            obj.MinPrice = criteria.MinPrice;
            obj.MaxPrice = criteria.MaxPrice;
            obj.BrandID = criteria.BrandID;
            obj.InStockOnly = criteria.InStockOnly;
            obj.NewOnly = criteria.NewOnly;
            if (criteria.Keywords == null)
            {
                obj.Keywords = "";
            }
            else
            {
                obj.Keywords = criteria.Keywords;
            }
            obj.SortBy = criteria.SortBy;
            obj.StartRow = criteria.StartRow;
            obj.EndRow = criteria.EndRow;

            //this.filtersselected = "";
            if (criteria.filtersselected != null && criteria.filtersselected != "")
            {
                Dictionary<string, string> Filters = new Dictionary<string, string>();
                String[] tmpFilters = criteria.filtersselected.Split(new[] { " AND " }, StringSplitOptions.None);
                foreach (String filter in tmpFilters)
                {
                    String[] data = filter.Split('=');
                    string Key = data[0].Trim();
                    string value = data[1].Replace("'", "").Trim();
                    Filters.Add(Key, value);
                }
                obj.filtersselected = JsonConvert.SerializeObject(Filters);
            }
            else
            {
                obj.filtersselected = "";
            }

            HttpCookie searchCookie = new HttpCookie("search", JsonConvert.SerializeObject(obj));
            Response.SetCookie(searchCookie);

        }

        [HttpGet]
        public ActionResult CategoryAbbv(string id)
        {
            int categoryid;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                try
                {
                    //get the userid
                    var p = new DynamicParameters();
                    p.Add("@Abbv", id);
                    categoryid = db.Query<int>("WholesaleHunterCom.sCategoryID", p, commandTimeout: 60, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
                }
                catch (Exception exc)
                {
                    categoryid = -1;
                }
            }

            if (categoryid == -1)
            {
                //category not found, redirect to home page
                return RedirectToAction("Index", "Home");
            }

            //now redirect to submit
            return RedirectToAction("Index", new { id = categoryid });
        }

        [HttpGet]
        public ActionResult Submit(SearchCriteria criteria)
        {
            int IsNewCustomer;
            int CustomerID;
            SearchViewModel vm;
            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
            }

            Dictionary<string, string> FiltersSelected = new Dictionary<string, string>();
            if (criteria.CategoryID != 0)
            {
                //now get the selected filter values
                StringBuilder ExtraFilters = new StringBuilder();
                if (criteria.filtersselected != null && criteria.filtersselected != string.Empty)
                {
                    var objects = JObject.Parse(criteria.filtersselected); // parse as array  
                    foreach (var filter in objects)
                    {
                        string filterName = filter.Key;
                        string filterValue = filter.Value == null ? "" : filter.Value.ToString();
                        FiltersSelected.Add(filterName, filterValue);
                        if (filterValue != "")
                        {
                            if (ExtraFilters.Length > 0)
                            {
                                ExtraFilters.Append(" AND ");
                            }
                            ExtraFilters.Append(String.Format("{0} = '{1}'", filterName, filterValue));
                        }
                    }
                }
                if (ExtraFilters.Length > 0)
                {
                    criteria.filtersselected = ExtraFilters.ToString();
                }
            }
            else
            {
                //clear FiltersSelected since they depend on CategoryID
                criteria.filtersselected = "";
            }

            if (criteria.Keywords != null)
            {
                //get rid of any leading or trailing spaces
                criteria.Keywords = criteria.Keywords.Trim();
                //now make sure there is only a single space between each word
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);
                criteria.Keywords = regex.Replace(criteria.Keywords, " ");
            }

            vm = getViewModel(criteria, IsNewCustomer);
            vm.SortBy = criteria.SortBy;
            vm.KeyWords = criteria.Keywords;
            vm.CategorySelected = criteria.CategoryID.ToString();
            vm.BrandSelected = criteria.BrandID.ToString();

            vm.InStockSelected = criteria.InStockOnly;
            vm.NewItemsSelected = criteria.NewOnly;

            vm.MinPrice = criteria.MinPrice;
            vm.MaxPrice = criteria.MaxPrice;
            vm.StartRow = criteria.StartRow;
            vm.EndRow = criteria.EndRow;
            vm.FiltersJSON = criteria.filtersselected;
            vm.FiltersSelected = FiltersSelected;

            vm.ScrollUrl = buildNextScrollingURL(criteria);

            SetLastSearchCookie(criteria);

            return View("Index", vm);
        }

        // GET: Search
        [HttpGet]
        public ActionResult Brand(int id)
        {
            int IsNewCustomer;
            int CustomerID;
            SearchViewModel vm;
            SearchCriteria criteria = new SearchCriteria();

            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
            }
            criteria.BrandID = id;
            vm = getViewModel(criteria, IsNewCustomer);
            vm.SortBy = "Popularity";
            vm.KeyWords = "";
            vm.CategorySelected = "0";
            vm.BrandSelected = criteria.BrandID.ToString();

            vm.InStockSelected = criteria.InStockOnly;
            vm.NewItemsSelected = criteria.NewOnly;

            vm.MinPrice = criteria.MinPrice;
            vm.MaxPrice = criteria.MaxPrice;
            vm.StartRow = criteria.StartRow;
            vm.EndRow = criteria.EndRow;
            //vm.List < FilterSelected > FiltersSelected = criteria.
            //vm.FiltersJSON = criteria.
            vm.ScrollUrl = buildNextScrollingURL(criteria);

            SetLastSearchCookie(criteria);
            return View("Index", vm);
        }

        // GET: Search
        [HttpGet]
        public ActionResult Index(string id)
        {
            int IsNewCustomer;
            int CustomerID;
            SearchViewModel vm;
            SearchCriteria criteria = new SearchCriteria();

            if (getClaims())
            {
                CustomerID = this.appuser.UserID;
                IsNewCustomer = 0;
            }
            else
            {
                CustomerID = 0;
                IsNewCustomer = 1;
            }

            criteria.CustomerID = CustomerID;
            criteria.Dept = "";

            string defaultSort = "Popularity";
            int catid;
            if (Int32.TryParse(id, out catid))
            {
                if (catid == -10)
                {
                    //clearance
                    defaultSort = "Clearance";
                    criteria.SortBy = "Clearance";
                    criteria.CategoryID = 0;
                }
                else
                {
                    criteria.CategoryID = catid;
                }
            }
            else
            {
                criteria.CategoryID = 0;
                criteria.Dept = id;
            }

            vm = getViewModel(criteria, IsNewCustomer);
            vm.SortBy = defaultSort;
            vm.KeyWords = criteria.Keywords;
            vm.CategorySelected = criteria.CategoryID.ToString();
            vm.BrandSelected = criteria.BrandID.ToString();

            vm.InStockSelected = criteria.InStockOnly;
            vm.NewItemsSelected = criteria.NewOnly;

            vm.MinPrice = criteria.MinPrice;
            vm.MaxPrice = criteria.MaxPrice;
            vm.StartRow = criteria.StartRow;
            vm.EndRow = criteria.EndRow;
            //vm.List < FilterSelected > FiltersSelected = criteria.
            //vm.FiltersJSON = criteria.
            vm.ScrollUrl = buildNextScrollingURL(criteria);

            SetLastSearchCookie(criteria);

            return View(vm);
        }

        private void SetMetadata(SearchViewModel vm, SearchCriteria criteria)
        {
            string hostname = ConfigurationManager.AppSettings["Host"].ToString();

            ViewBag.Description = vm.MetaData.HTMLDescription;
            ViewBag.Title = vm.MetaData.HTMLTitle;
            ViewBag.Keywords = vm.MetaData.HTMLKeywords;
            vm.OG_Description = vm.MetaData.HTMLDescription;
            vm.OG_Title = vm.MetaData.HTMLTitle;
            vm.OG_Image = hostname + "images/wholesalehunter.jpg";
            vm.OG_Url = hostname;

            if (criteria.CategoryID == -10)
            {
                vm.CategorySelectedName = "Clearance";
            }
            else if (criteria.CategoryID == 0)
            {
                if(criteria.Dept != string.Empty)
                {
                    vm.CategorySelectedName = criteria.Dept;
                }

                else if (criteria.BrandID == 0)
                {
                    vm.CategorySelectedName = "All Categories";

                }
            }
            else
            {
                vm.CategorySelectedName = "";
            }
        }

        private string buildNextScrollingURL(SearchCriteria data)
        {
            int newStartRow = data.EndRow + 1;
            int newEndRow = newStartRow + 100;
            StringBuilder url = new StringBuilder();
            string hostname = ConfigurationManager.AppSettings["Host"].ToString();
            if (!hostname.EndsWith("/"))
            {
                hostname = hostname + "/";
            }
            url.Append(hostname);
            url.Append("Search/Scroll?CategoryID=");
            url.Append(data.CategoryID);
            url.Append("&MinPrice=");
            url.Append(data.MinPrice);
            url.Append("&MaxPrice=");
            url.Append(data.MaxPrice);
            url.Append("&BrandID=");
            if (data.BrandID != 0)
            {
                url.Append(data.BrandID);
            }
            url.Append("&InStockOnly=");
            url.Append(data.InStockOnly);
            url.Append("&NewOnly=");
            url.Append(data.NewOnly);
            url.Append("&Keywords=");
            if (data.Keywords != null)
            {
                url.Append(HttpUtility.UrlEncode(data.Keywords));
            }
            url.Append("&SortBy=");
            url.Append(HttpUtility.UrlEncode(data.SortBy));
            url.Append("&StartRow=");
            url.Append(newStartRow);
            url.Append("&EndRow=");
            url.Append(newEndRow);
            url.Append("&filtersselected=");
            if (data.filtersselected != null)
            {
                url.Append(HttpUtility.UrlEncode(data.filtersselected));
            }
            url.Append("&CustomerID=");
            url.Append(data.CustomerID);
            if (data.Dept != string.Empty)
            {
                url.Append("&Dept=");
                url.Append(data.Dept);
            }
            return url.ToString();
        }

        private SearchViewModel getViewModel(SearchCriteria criteria, int IsNewCustomer)
        {
            SearchViewModel vm = new SearchViewModel();
            int custid;
            if (Int32.TryParse(ViewBag.CustomerID.ToString(), out custid))
            {
                vm.CustomerID = custid;
            }
            else
            {
                vm.CustomerID = 0;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //get the userid
                var p = new DynamicParameters();
                if (criteria.CategoryID == 0)
                {
                    p.Add("@CategoryID", (int?)null);
                    //the category has been cleared so we need to make sure the extra filters
                    //are cleared too since the extra filters are depending upon the category
                    criteria.filtersselected = "";
                }
                else
                {
                    p.Add("@CategoryID", criteria.CategoryID);
                }

                if (criteria.MinPrice == 0m)
                {
                    p.Add("@MinPrice", (int?)null);
                }
                else
                {
                    p.Add("@MinPrice", criteria.MinPrice);
                }

                if (criteria.MaxPrice == 0m)
                {
                    p.Add("@MaxPrice", (int?)null);
                }
                else
                {
                    p.Add("@MaxPrice", criteria.MaxPrice);
                }

                if (criteria.BrandID == 0)
                {
                    p.Add("@BrandID", (int?)null);
                }
                else
                {
                    p.Add("@BrandID", criteria.BrandID);
                }
                if (criteria.InStockOnly)
                {
                    p.Add("@InStockOnly", true);
                }
                else
                {
                    p.Add("@InStockOnly", (bool?)null);
                }

                if (criteria.NewOnly) {
                    p.Add("@NewOnly", true);
                }
                else
                {
                    p.Add("@NewOnly", (bool?)null);
                }

                if (criteria.Keywords == string.Empty)
                {
                    p.Add("@Keywords", null);
                }
                else
                {
                    p.Add("@Keywords", criteria.Keywords);

                }

                p.Add("@SortBy", criteria.SortBy);
                p.Add("@StartRow", criteria.StartRow);
                p.Add("@EndRow", criteria.EndRow);
                if (criteria.filtersselected != "")
                {
                    p.Add("@ExtraFilters", criteria.filtersselected);
                }
                else
                {
                    p.Add("@ExtraFilters", null);
                }

                p.Add("@CustomerID", criteria.CustomerID);
                if (criteria.Dept != string.Empty)
                {
                    p.Add("@Dept", criteria.Dept);
                }

                var results = db.QueryMultiple("WholesaleHunterCom.sProductSearchResults", p, commandTimeout: 60, commandType: CommandType.StoredProcedure);

                vm.Products = results.Read<SearchProduct>().ToList();
                vm.ResultCount = results.Read<SearchResultCount>().FirstOrDefault();
                vm.Category = results.Read<SearchCategory>().ToList();
                vm.Brands = results.Read<SearchBrand>().ToList();
                List<SearchFilter> listFilter = results.Read<SearchFilter>().ToList();
                vm.MetaData = results.Read<HTMLMetaData>().First<HTMLMetaData>();

                //get the top 10 categories and select them to display
                List<SearchCategory> categories = new List<SearchCategory>();
                categories = vm.Category.OrderBy(o => o.ResultCount).ToList<SearchCategory>();
                categories.Reverse();
                for (int cnt = 0; cnt < categories.Count; cnt++)
                {
                    if (cnt < 10)
                    {
                        categories[cnt].CssClass = "filterAlwaysVisible";
                    }
                    else
                    {
                        categories[cnt].CssClass = "filterHidden";
                    }
                }
                vm.Category = categories.OrderBy(o => o.CategoryName).ToList<SearchCategory>();


                //get the top 5 brands and select them to display
                List<SearchBrand> brands = new List<SearchBrand>();
                brands = vm.Brands.OrderBy(o => o.ResultCount).ToList<SearchBrand>();
                brands.Reverse();
                for (int cnt = 0; cnt < brands.Count; cnt++)
                {
                    if (cnt < 5)
                    {
                        brands[cnt].BrandClass = "filterAlwaysVisible";
                    }
                    else
                    {
                        brands[cnt].BrandClass = "filterHidden";
                    }
                }
                vm.Brands = brands.OrderBy(o => o.BrandName).ToList<SearchBrand>();

                if (listFilter.Count>0)
                {
                    int cntFilterGroups = 1;
                    string prevFilterName = "";
                    List<SearchFilter> holder = null;
                    int cntHolder = 0;
                    foreach (SearchFilter filter in listFilter)
                    {
                        if (filter.FilterName != prevFilterName)
                        {
                            if (holder != null)
                            {
                                //add holder to the view model filters
                                vm.Filters.Add(holder);
                                vm.FilterGroupNames.Add(cntFilterGroups++, prevFilterName);
                            }
                            holder = new List<SearchFilter>();
                            cntHolder = 0;
                            prevFilterName = filter.FilterName;
                        }
                        if (!(filter.AttributeID == null || filter.AttributeName == null))
                        {
                            filter.cntFilter = cntHolder++;
                            filter.filterClass = "filterHidden";
                            holder.Add(filter);
                        }
                    }
                    if (holder != null)
                    {
                        vm.Filters.Add(holder);
                        vm.FilterGroupNames.Add(cntFilterGroups++, prevFilterName);
                    }
                }

                List<SearchFilter> filters = new List<SearchFilter>();
                int maxCount = 0;
                //now we need to get the top 5 in each filter group
                for (int i = 0; i < vm.Filters.Count; i++)
                {
                    filters = vm.Filters[i].OrderBy(o => o.ResultCount).ToList<SearchFilter>();
                    filters.Reverse();
                    if (filters.Count == 1)
                    {
                        filters[0].filterClass = "filterAlwaysVisible";
                    }
                    else
                    {
                        if (filters.Count < 5)
                        {
                            maxCount = filters.Count;
                        }
                        else
                        {
                            maxCount = 5;
                        }

                        for (int cnt = 0; cnt < maxCount; cnt++)
                        {
                            filters[cnt].filterClass = "filterAlwaysVisible";
                        }
                    }
                    vm.Filters[i] = filters.OrderBy(o => o.AttributeName).ToList<SearchFilter>();
                }

                //now get the specials
                p = new DynamicParameters();
                p.Add("@IsNewCustomer", IsNewCustomer);
                p.Add("@Category", criteria.CategoryID.ToString());
                var spec = db.Query<Special>("WholesaleHunterCom.sSpecials", p, commandType: CommandType.StoredProcedure);
                vm.Specials = spec.ToList<Special>();

                //now get the cart preview
                p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.Cart = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            SetMetadata(vm, criteria);

            if (criteria.BrandID > 0)
            {
                vm.BrandSelectedName = getSelectedBrandName(vm, criteria.BrandID);
            }
            if (criteria.Dept != string.Empty)
            {
                vm.Dept = criteria.Dept;
            }
            return vm;
        }

        private string getSelectedCategoryName(SearchViewModel vm, int CategoryID)
        {
            string CategoryName = "";
            foreach (SearchCategory cat in vm.Category)
            {
                if (cat.CategoryID == CategoryID)
                {
                    CategoryName = cat.CategoryName;
                    break;
                }
                if (cat.ParentCategoryID == CategoryID)
                {
                    CategoryName = cat.ParentCategoryName;
                    break;
                }
            }
            return CategoryName;
        }

        private string getSelectedBrandName(SearchViewModel vm, int BrandID)
        {
            string BrandName = "";
            foreach (SearchBrand brand in vm.Brands)
            {
                if (brand.BrandID == BrandID)
                {
                    BrandName = brand.BrandName;
                    break;
                }
            }
            return BrandName;
        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }
    }
}