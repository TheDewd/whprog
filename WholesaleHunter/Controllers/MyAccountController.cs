﻿using Dapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WholesaleHunter.Classes;
using WholesaleHunter.Models;
using WholesaleHunter.ViewModels;


namespace WholesaleHunter.Controllers
{
    [OutputCache(Duration = 1)]
    [Authorize]
    public class MyAccountController : Controller
    {
        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";



        // GET: MyAccount
        public ActionResult Index(MyAccountSelectedTab sTab)
        {

            MyAccountViewModel vm = new MyAccountViewModel();
            vm.SelectedTab = sTab.SelectedTab;
            if (getClaims())
            {
                vm.CustomerID = this.appuser.UserID;
            }

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", vm.CustomerID);
                vm.MyInfo = db.Query<MyInfo>("WholesaleHunterCom.sMyInfo", p, commandType: CommandType.StoredProcedure).Single<MyInfo>();
            }

            ViewBag.CustomerID = vm.CustomerID;
            return View(vm);
        }

        public ActionResult MyAddresses(int userID)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                MyAccountViewModel vm = new MyAccountViewModel();

                //Get My Addresses
                var pMA = new DynamicParameters();
                pMA.Add("@CustomerID", userID);
                vm.CustomerAddress = db.Query<MyAddress>("WholesaleHunterCom.sMyAddresses", pMA, commandType: CommandType.StoredProcedure).ToList<MyAddress>();
                ViewBag.CustomerID = userID;
                return View(vm);
            }

        }

        public ActionResult MyPaymentMethods(int userID)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                MyAccountViewModel vm = new MyAccountViewModel();

                //Get My Addresses
                var pMA = new DynamicParameters();
                pMA.Add("@CustomerID", userID);
                vm.CustomerPaymentMethod = db.Query<MyPaymentMethod>("WholesaleHunterCom.sMyPaymentMethods", pMA, commandType: CommandType.StoredProcedure).ToList<MyPaymentMethod>();
                ViewBag.CustomerID = userID;
                return View(vm);
            }

        }

        public ActionResult MyContactPreferences(int userID)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                MyAccountViewModel vm = new MyAccountViewModel();

                //Get My Contact Preferences
                var pMCP = new DynamicParameters();
                pMCP.Add("@CustomerID", userID);

                Customer customer = db.Query<Customer>("WholesaleHunterCom.sMyContactPreferences", pMCP, commandType: CommandType.StoredProcedure).FirstOrDefault();
                vm.Phone = customer.Phone;
                vm.EMail = customer.EMail;
                vm.GetNewsletter = customer.GetNewsletter;
                vm.CustomerID = userID;

                ViewBag.CustomerID = userID;

                return View(vm);
            }
        }

        public ActionResult MyPassword(int userID)
        {
            ViewBag.CustomerID = userID;
            return View();
        }

        public ActionResult MyOrderHistory(int userID, int startRow = 1, int EndRow = 15)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                MyAccountViewModel vm = new MyAccountViewModel();

                //Get My Addresses
                var pOH = new DynamicParameters();
                pOH.Add("@CustomerID", userID);
                pOH.Add("@StartRow", startRow);
                pOH.Add("@EndRow", EndRow);
                vm.MyOrderHistory = db.Query<MyOrderHistory>("WholesaleHunterCom.sMyOrders", pOH, commandType: CommandType.StoredProcedure).ToList<MyOrderHistory>();
                ViewBag.CustomerID = userID;

                if (vm.MyOrderHistory.Count > 0)
                {
                    //Build the scrolling url for the next group of orders
                    MyOrdersScroll myOrdersScroll = new MyOrdersScroll();
                    myOrdersScroll.StartNum = startRow;
                    myOrdersScroll.EndNum = EndRow;
                    vm.OrderScrollUrl = buildNextOrderScrollingURL(userID, myOrdersScroll);
                    vm.HasOrderHistory = true;
                }
                else
                {
                    vm.OrderScrollUrl = string.Empty;
                    if (startRow == 1)
                    {
                        vm.HasOrderHistory = false;
                    }
                    else
                    {
                        vm.HasOrderHistory = true;
                    }
                }
                return View(vm);
            }
        }

        private string buildNextOrderScrollingURL(int UserID, MyOrdersScroll data)
        {
            int newStartRow = data.EndNum + 1;
            int newEndRow = newStartRow + 15;
            StringBuilder url = new StringBuilder();
            string hostname = ConfigurationManager.AppSettings["Host"].ToString();
            if (!hostname.EndsWith("/"))
            {
                hostname = hostname + "/";
            }
            url.Append(hostname);
            url.Append("MyAccount/MyOrderHistory?UserID=");
            url.Append(UserID);
            url.Append("&StartRow=");
            url.Append(newStartRow);
            url.Append("&EndRow=");
            url.Append(newEndRow);


            return url.ToString();

        }

        public ActionResult RecentlyViewed(int userID, int startRow = 1, int endRow = 5)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                MyAccountViewModel vm = new MyAccountViewModel();

                //Get My Contact Preferences
                var pRV = new DynamicParameters();
                pRV.Add("@CustomerID", userID);
                pRV.Add("@StartRow", startRow);
                pRV.Add("@EndRow", endRow);

                vm.RecentlyViewed = db.Query<RecentlyViewed>("WholesaleHunterCom.sRecentlyViewed", pRV, commandType: CommandType.StoredProcedure).ToList<RecentlyViewed>();

                ViewBag.CustomerID = userID;

                return View(vm);
            }
        }

        public ActionResult MyAccountLogout()
        {
            MyAccountViewModel vm = new MyAccountViewModel();
            getClaims();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                //Get My Contact Preferences
                var pRV = new DynamicParameters();
                pRV.Add("@CustomerID", appuser.UserID);

                vm.MyInfo = db.Query<MyInfo>("WholesaleHunterCom.sMyInfo", pRV, commandType: CommandType.StoredProcedure).FirstOrDefault();

                ViewBag.CustomerID = appuser.UserID;
            }

            return View(vm);
        }

        //public ActionResult MyAccountLogOff()
        //{
        //    IdentitySignout();
        //    return RedirectToAction("Index","Home");

        //}


        public void IdentitySignout()
        {
            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalCookie);
        }
        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                ViewBag.email = appuser.Email;
                ViewBag.username = appuser.Email;
                ViewBag.userid = appuser.UserID;
                ViewBag.customerid = appuser.UserID;
                status = true;
            }
            catch
            {
                ViewBag.email = "";
                ViewBag.username = "";
                ViewBag.userid = "";
                ViewBag.customerid = "";
                status = false;
            }
            return status;
        }
    }
}