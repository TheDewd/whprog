﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartItemUpdate
    {
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public decimal Price { get; set; }
        public int NewQuantity { get; set; }
    }
}