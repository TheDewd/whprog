﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ProductDetails
    {
        public string Title { get; set; }
        public int MfgID { get; set; }
        public string MfgNo { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public decimal MSRP { get; set; }
        public decimal Price { get; set; }
        public decimal UnitPrice { get; set; }
        public int QuantityTotal { get; set; }
        public int QuantityAtThisPrice { get; set; }
        public string mfgName { get; set; }
        public string mfgPhone { get; set; }
        public string mfgEmail { get; set; }
        public string mfgWebsite { get; set; }
        public string mfgAddress1 { get; set; }
        public string mfgAddress2 { get; set; }
        public string mfgCityStateZip { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public int FreeShipping { get; set; }
        public int FlatShipping { get; set; }
        public string DiscountMessage { get; set; }
        public string NoHTMLDescription { get; set; }
        public int ProductID { get; set; }
    }
}