﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class MyAddress
    {
        public int CustomerAddressID { get; set; }

        public int CustomerID { get; set; }

        public string AddressName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZIPCode { get; set; }

        public bool IsPrimary { get; set; }
    }
}