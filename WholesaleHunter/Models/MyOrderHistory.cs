﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class MyOrderHistory
    {
        public int ProductID { get; set; }

        public string ItemTitle { get; set; }

        public string ItemDescriptionShort { get; set; }

        public string Thumbnail { get; set; }

        public DateTime OrderDate { get; set; }

        public int Quantity { get; set; }

        public string OrderStatus { get; set; }

        public string TrackingNumber { get; set; }

        public string Carrier { get; set; }

        public double Price { get; set; }


    }
}