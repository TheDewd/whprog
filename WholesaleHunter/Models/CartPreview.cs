﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartPreview
    {
        public int TotalQuantity { get; set; }
        public decimal TotalItemPrice { get; set; }
    }
}