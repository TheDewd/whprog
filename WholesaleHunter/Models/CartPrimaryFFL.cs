﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartPrimaryFFL
    {
        public int FFLID { get; set; }
        public string FFLName { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string FFLNumber { get; set; }

        public CartPrimaryFFL()
        {
            this.FFLID = 0;
            this.FFLName = "";
            this.Address1 = "";
            this.City = "";
            this.State = "";
            this.ZIPCode = "";
            this.Email = "";
            this.ContactName = "";
            this.Phone = "";
            this.FFLNumber = "";

        }
    }


}