﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ProductDetailsImages
    {
        public string Picture { get; set; }
        public int SortOrder { get; set; }
    }
}