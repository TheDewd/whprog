﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchFilter
    {
        public string FilterName { get; set; }
        public string AttributeID { get; set; }
        public string AttributeName { get; set; }
        public int ResultCount { get; set; }
        public string filterClass { get; set; }
        public int cntFilter { get; set; }
    }
}