﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class PricePoints
    {
        public decimal WHPrice { get; set; }
        public int Qty { get; set; }
        public int Check { get; set; }
    }
}