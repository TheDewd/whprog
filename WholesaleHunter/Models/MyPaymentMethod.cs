﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class MyPaymentMethod
    {
        public int CustomerPaymentMethodID { get; set; }

        public int CustomerID { get; set; }

        public string LastFour { get; set; }

        public string AuthorizeNETToken { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool IsPrimary { get; set; }

        public string BillAddr1 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillZIP { get; set; }

        //These are not stored anywhere on this site.  They are sent to Authorize.net
        public string PaymentMethodNumber { get; set; }

        public string PaymentMethodCVV { get; set; }

        public string BillingZipCode { get; set; }


        //Used to construct Expiration Date
        public string ExpirationMonth { get; set; }

        public string ExpirationYear { get; set; }

    }
}