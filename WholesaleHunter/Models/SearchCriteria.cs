﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchCriteria
    {
        public int CategoryID { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int BrandID { get; set; }
        public bool InStockOnly { get; set; }
        public bool NewOnly { get; set; }
        public string Keywords { get; set; }
        public string SortBy { get; set; }
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public string filtersselected { get; set; }
        public int CustomerID { get; set; }
        public string Dept { get; set; }
        
        public SearchCriteria()
        {
            this.CategoryID = 0;
            this.MinPrice = 0m;
            this.MaxPrice = 0m;
            this.BrandID = 0;
            this.InStockOnly = false;
            this.NewOnly = false;
            this.Keywords = "";
            this.SortBy = "Popularity";
            this.StartRow = 1;
            this.EndRow = 100;
            this.filtersselected = "";
            this.CustomerID = 0;
            this.Dept = "";
        }
    }
}