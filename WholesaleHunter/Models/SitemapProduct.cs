﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SitemapProduct
    {
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int ProductID { get; set; }
    }
}