﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class HTMLMetaData
    {
        public string HTMLTitle { get; set; }
        public string HTMLDescription { get; set; }
        public string HTMLKeywords { get; set; }
    }
}