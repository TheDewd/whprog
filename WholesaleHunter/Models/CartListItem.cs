﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartListItem
    {
        public int ProductID { get; set; }
        public string ItemTitle { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int? AuctionID { get; set; }
    }
}