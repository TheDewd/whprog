﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchBrand
    {
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public int ResultCount { get; set; }
        public string BrandClass { get; set; }
    }
}