﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class Special
    {
        public string ImageLocation { get; set; }
        public string SpecialText { get; set; }
        public string LinkTarget { get; set; }

    }
}