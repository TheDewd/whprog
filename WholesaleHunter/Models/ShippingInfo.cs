﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ShippingInfo
    {
        public int CustomerID { get; set; }
        public string ZipCode { get; set; }
    }
}