﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class NewsScroll
    {
        public int StartNum { get; set; }

        public int EndNum { get; set; }
    }
}