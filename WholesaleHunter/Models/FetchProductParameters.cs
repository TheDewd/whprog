﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class FetchProductParameters
    {
        public int SliderID { get; set; }
        public string SliderDirection { get; set; }
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public int CategoryID { get; set; }
        public int CustomerID { get; set; }

    }
}