﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchSubmit
    {
        public string sortbyselected { get; set; }
        public string keywords { get; set; }
        public string categoryselected { get; set; }
        public string brandselected { get; set; }
        public string instockselected { get; set; }
        public string newitemsselected { get; set; }
        public string minprice { get; set; }
        public string maxprice { get; set; }
        public string startrow { get; set; }
        public string endrow { get; set; }
        public string filtersselected { get; set; }
    }
}