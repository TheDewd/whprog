﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class Logon
    {
        public string username { get; set; }
        public string password { get; set; }
        public string forgotPW { get; set; }
    }
}