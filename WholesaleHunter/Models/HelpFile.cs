﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class HelpFile
    {
        public string Category { get; set; }

        public string Topic { get; set; }

        public string HelpText { get; set; }


    }
}