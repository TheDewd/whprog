﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchResultCount
    {
        public int ResultCount { get; set; }
    }
}