﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class RecentlyViewed
    {
        public string ItemTitle { get; set; }

        public string LinkTarget { get; set; }

        public string ImageLocation { get; set; }

        public string ImageDescriptionShort { get; set; }

        public decimal Price { get; set; }

    }
}