﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CustomerGUID {
        public int CustomerID { get; set; }
        public string Email { get; set; }
    }
}