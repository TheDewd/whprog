﻿$(function () {


});

function showManufacturers() {

    //Display the overlay
    $("#helpMask").show();

    //Display the Manufacturers
    //$("#manufacturerList").show();
    $("#showManufacturers").dialog({
        position: { my: "center top", at: "center-360 top+20", of: window },
        dialogClass: 'manufacturerPopup',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });

    return false;
};

function closeManufacturerList() {
    $("#helpMask").hide();
    $("#showManufacturers").dialog('close')
};