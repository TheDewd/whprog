﻿$(function () {

 

});

function showHelpText(popupDiv) {
    //Construct the name of the Div that will be displayed
    
    var popupTextDiv = "#" + popupDiv;
    //Display the overlay
    $("#helpMask").show();

    $(popupTextDiv).on('dialogclose', function (event) {
        closeHelpText()
    });
    
    var $dialog = $(popupTextDiv);

    $dialog.dialog({
        height: 'auto',
        minWidth: 600,
        width: 600,
        maxWidth: 600,
        dialogClass: 'helpText',
        position: { my: "center top", at: "center top+150", of: window }
    });
    //$dialog.html(helpTextDisplay);
       
    return false;
};

   

function closeHelpText(popupDiv) {
    //Construct the name of the Div that will be displayed
    $("#" + popupDiv).dialog('close');
    $("#helpMask").hide();
    return false;
};


function closeHelpSearch() {
    $("#helpMask").hide();
    $("#searchTextList").dialog('close')
};


function searchHelpText() {
    var keywords = $("#keywordsearch2").val();
    if (keywords == "") {
        alert("Please enter the keywords to search for.");
        return false;
    }

       var resultURL = "/Home/HelpSearchResults?searchText=" + keywords 
    //Display the overlay
    $("#helpMask").show();

    $.ajax({
        cache: false,
        url: resultURL,
        
        success: function (result) {
            
            $("#searchTextList").html(result);
        }
            

    });

    $("#searchTextList").dialog({
        position: {
            my: "center top",
            at: "center-360 top+20",
            of: window
        },
        dialogClass: 'searchTextList',
        closeOnEscape: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar", ui.dialog | ui).hide();
        }
        });
    return false;
}