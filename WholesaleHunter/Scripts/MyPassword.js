﻿function updatePassword() {


    var password2 = $("#password2").val();
    var password3 = $("#password3").val();

    if (password2 != password3)
    {
        alert("Passwords do not match!");
        //apply the wrong password class
        document.getElementById("password2").className = "wrongPassword";  
        document.getElementById("password3").className = "wrongPassword";
    }
    else
    {
        //remove the wrong password class
        document.getElementById("password1").className = document.getElementById("password2").className.replace(/(?:^|\s)wrongPassword(?!\S)/g, '')
        document.getElementById("password2").className = document.getElementById("password2").className.replace(/(?:^|\s)wrongPassword(?!\S)/g, '')
        document.getElementById("password3").className = document.getElementById("password3").className.replace(/(?:^|\s)wrongPassword(?!\S)/g, '')

        //now validate the password
        result = validatePassword($("#password2").val(), $("#pwEmail").val(), $("#pwFirstName").val(), $("#pwLastName").val());
        /*return values
        -1 malformed email
        -2 password is too short
        -3 password is not complicated enough
        -4 pasword starts or ends with a space
        1 password is valid
        */
        if (result == -1 || result == -2 || result == -3) {
            alert('That password is too easy to guess.  Try making it longer, adding numbers, symbols, or upper and lower case letters.');
            return false;
        }
        else if (result == -4) {
            alert('You entered invalid characters.  A password cannot start or end with a space.');
            $("#Password").val('');
            $("#VerifyPassword").val('');
            return false;
        }

        if (confirm("Are you sure you want to change your password?")) {
            var passwords = {
                Password1: $("#password1").val(),
                Password2: $("#password2").val(),
                CustomerID: $("#customerID").val()
            }
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "/api/AJax/UpdatePassword",
            data: JSON.stringify(passwords),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data == "ok") {
                    alert("Your password has been updated.")
                    showMyAccount(4);
                }
                else if(data == "wrongpassword")
                {
                    alert("Your current password is not correct");
                    //apply the wrong password class
                    document.getElementById("password1").className = "wrongPassword";
                    return false;
                }
                else {
                    alert("An error was encountered attempting to update your password. Please try again and if the error persists please contact our customer service department.");
                    return false;
                }
            }
        });
    }
    
};