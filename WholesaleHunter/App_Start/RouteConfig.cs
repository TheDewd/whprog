﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WholesaleHunter
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "SearchByKeyword",
                url: "Search/Keyword/",
                defaults: new { controller = "Search", action = "Keyword" }
            );

            routes.MapRoute(
                name: "SearchByForm",
                url: "Search/Submit/",
                defaults: new { controller = "Search", action = "Submit" }
            );

            routes.MapRoute(
                name: "SearchLast",
                url: "Search/Last/",
                defaults: new { controller = "Search", action = "Last" }
            );

            routes.MapRoute(
                name: "SearchScroll",
                url: "Search/Scroll/",
                defaults: new { controller = "Search", action = "Scroll" }
            );

            routes.MapRoute(
                name: "Search",
                url: "Search/{id}",
                defaults: new { controller = "Search", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Cart",
                url: "Cart/Contents",
                defaults: new { controller = "Cart", action = "Contents", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
