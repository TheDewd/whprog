﻿using System.Web;
using System.Web.Optimization;

namespace WholesaleHunter
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.min.js",
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jscroll").Include(
                        "~/Scripts/jquery.jscroll.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jscookie").Include(
                        "~/Scripts/js.cookie.js"));

            bundles.Add(new ScriptBundle("~/bundles/wholesalehunter").Include(
                      "~/Scripts/ism-2.2.min.js",
                      "~/Scripts/SpryMenuBar.js",
                      "~/Scripts/index.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/my-slider.css",
                      "~/Content/SpryMenuBarHorizontal.css",
                      "~/Content/search.css",
                      "~/Content/style.css"));

            bundles.Add(new StyleBundle("~/Content/tabs").Include(
                      "~/Content/SpryTabbedPanels.css"));

            bundles.Add(new ScriptBundle("~/bundles/tabs").Include(
                    "~/Scripts/SpryTabbedPanels.js"));

            bundles.Add(new ScriptBundle("~/bundles/searchpage").Include(
                    "~/Scripts/search.script.js"));

            bundles.Add(new StyleBundle("~/Content/searchpage").Include(
                    "~/Content/searchstyle.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                    "~/Content/bootstrap-theme.min.css",
                    "~/Content/bootstrap.min.css"));
        }
    }
}
